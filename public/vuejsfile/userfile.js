const App = {
    data() {
      return {
        title: 'testing',
        kabupaten : [],
        provinsi : '',
        ongkir : [],
        kab_kota : '',
        ongkos_kirim : 0,
        courier : '',
        loadCekongkir : false,
      }
    },
    methods : {
      formatPrice: function (value) {

        let val = (value / 1).toFixed(0).replace('.', ',')
        return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
      },
      getKabupaten(){
        const url = general.base_url
        axios.post(`${url}/api/getKabupatenrajaongkir` , {
          provinsi : this.provinsi,
        }).then(response => {

          this.kabupaten = response.data;
          //console.log(response.data);

        });
        
      },
      showOngkir(){
        this.loadCekongkir = true;
        const url = general.base_url
        const kabkota = this.kab_kota;
        const kurir = this.courier;

        axios.post(`${url}/api/ongkoskirim` , {
          kabkota : kabkota,
          kurir : kurir,
        }).then(response => {

          this.ongkir =  response.data.costs;
         console.log(response.data.costs);
          this.loadCekongkir = false;
        });

      },
      jumlahTotal(){
        return this.formatPrice(totalpembayaran.total + this.ongkos_kirim);
      }
    },
    mounted(){
      this.jumlahTotal();
    }
  }
  
  Vue.createApp(App).mount('#app')
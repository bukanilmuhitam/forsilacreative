// Modal
// Get the button that opens the modal
var btn = document.getElementById("modal");
btn.onclick = function(){
   var value = btn.dataset;
   var modal = document.getElementById(value.id);
   modal.style.display = 'block'
}

var span = document.getElementsByClassName('close')[0];
span.onclick = function() {
    var getmodal = document.getElementsByClassName('modal');
   getmodal[0].style.display = 'none';
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    var getmodal = document.getElementsByClassName('modal');
    if(event.target === getmodal[0]){
        getmodal[0].style.display = 'none';
    }
}
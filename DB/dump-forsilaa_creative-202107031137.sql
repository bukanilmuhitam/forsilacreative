-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: localhost    Database: forsilaa_creative
-- ------------------------------------------------------
-- Server version	5.5.5-10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_user`
--

DROP TABLE IF EXISTS `admin_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_user`
--

LOCK TABLES `admin_user` WRITE;
/*!40000 ALTER TABLE `admin_user` DISABLE KEYS */;
INSERT INTO `admin_user` VALUES (1,'admin','727ac653dfda2e10ec9a95a2e95a37f2');
/*!40000 ALTER TABLE `admin_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cart_produk`
--

DROP TABLE IF EXISTS `cart_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cart_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_product` int(11) DEFAULT NULL,
  `username` text DEFAULT NULL,
  `qty` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cart_produk`
--

LOCK TABLES `cart_produk` WRITE;
/*!40000 ALTER TABLE `cart_produk` DISABLE KEYS */;
INSERT INTO `cart_produk` VALUES (19,3,'ashri29410',2),(20,1,'ashri29410',1),(21,3,'Hari68707',1);
/*!40000 ALTER TABLE `cart_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `category_product`
--

DROP TABLE IF EXISTS `category_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_category` varchar(30) DEFAULT NULL,
  `permalink` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category_product`
--

LOCK TABLES `category_product` WRITE;
/*!40000 ALTER TABLE `category_product` DISABLE KEYS */;
INSERT INTO `category_product` VALUES (3,'foto & hadiah','foto-&-hadiah'),(4,'events','events'),(5,'peralatan kantor','peralatan-kantor'),(6,'kemasan','kemasan'),(7,'materials marketing','materials-marketing');
/*!40000 ALTER TABLE `category_product` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gambar_lainnya`
--

DROP TABLE IF EXISTS `gambar_lainnya`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gambar_lainnya` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_produk` int(11) DEFAULT NULL,
  `nama_gambar_lainnya` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gambar_lainnya`
--

LOCK TABLES `gambar_lainnya` WRITE;
/*!40000 ALTER TABLE `gambar_lainnya` DISABLE KEYS */;
/*!40000 ALTER TABLE `gambar_lainnya` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `list_produk`
--

DROP TABLE IF EXISTS `list_produk`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `list_produk` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_category` int(11) DEFAULT NULL,
  `nama_produk` varchar(30) DEFAULT NULL,
  `materials` varchar(30) DEFAULT NULL,
  `kuantitas` int(11) DEFAULT NULL,
  `estimasi_selesai_produksi` int(11) DEFAULT NULL,
  `gambar_produk` text DEFAULT NULL,
  `deskripsi_produk` text DEFAULT NULL,
  `type_ukuran` int(11) DEFAULT NULL,
  `ukuran_produk` varchar(10) DEFAULT NULL,
  `harga_default_pcs` float DEFAULT NULL,
  `permalink_product` text DEFAULT NULL,
  `berat_product` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `list_produk`
--

LOCK TABLES `list_produk` WRITE;
/*!40000 ALTER TABLE `list_produk` DISABLE KEYS */;
INSERT INTO `list_produk` VALUES (1,17,'papper bag kemasan makanan','kertas papper makanan',0,30,'image_2021011308584355567.jpg','',0,'20 x 20',250,'papper-bag-kemasan-makanan',40),(3,6,'kado untuk hadiah wisuda','foto, bingkai , kertas photo',0,10,'image_2021011410042152283.jpg','',0,'20 x 20',50000,'kado-untuk-hadiah-wisuda',50);
/*!40000 ALTER TABLE `list_produk` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `materials`
--

DROP TABLE IF EXISTS `materials`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `materials` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama_material` varchar(30) DEFAULT NULL,
  `gambar_material` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `materials`
--

LOCK TABLES `materials` WRITE;
/*!40000 ALTER TABLE `materials` DISABLE KEYS */;
/*!40000 ALTER TABLE `materials` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifikasi`
--

DROP TABLE IF EXISTS `notifikasi`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `notifikasi` (
  `id` int(11) NOT NULL,
  `username` varchar(30) DEFAULT NULL,
  `pesan_notifikasi` text DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifikasi`
--

LOCK TABLES `notifikasi` WRITE;
/*!40000 ALTER TABLE `notifikasi` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifikasi` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_order`
--

DROP TABLE IF EXISTS `product_order`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number_invoice` varchar(50) DEFAULT NULL,
  `product_id` text DEFAULT NULL,
  `username` text DEFAULT NULL,
  `alamat_pengiriman` text DEFAULT NULL,
  `provinsi` text DEFAULT NULL,
  `kabupaten` text DEFAULT NULL,
  `kecamatan` text DEFAULT NULL,
  `kode_pos` varchar(10) DEFAULT NULL,
  `keterangan_tambahan_pengiriman` text DEFAULT NULL,
  `tshipping` float DEFAULT NULL,
  `tpayment` float DEFAULT NULL,
  `catatan_tambahan` text DEFAULT NULL,
  `status_order` int(11) DEFAULT NULL,
  `date_order` date DEFAULT NULL,
  `qty_perproduct` text DEFAULT NULL,
  `lampiran_file` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_order`
--

LOCK TABLES `product_order` WRITE;
/*!40000 ALTER TABLE `product_order` DISABLE KEYS */;
INSERT INTO `product_order` VALUES (4,'INVOICE-1935','3,1','ashri29410','Jl. Pringgan Pasar 2 Helvetia','Sumatera Utara','Deli Serdang','Sunggal','20124','',30000,80250,'',3,'2021-01-18','1,1',NULL),(5,'INVOICE-878','3','ashri29410','Jl. Pringgan Pasar 2 Helvetia','Sumatera Utara','Deli Serdang','Sunggal','20124','',30000,80000,'tolong perbanyak warna merah',3,'2021-01-23','1','image_2021012308133462595.jpg'),(6,'INVOICE-1117','3','ashri29410','Jl. Pringgan Pasar 2 Helvetia','Sumatera Utara','Deli Serdang','Sunggal','20124','ada ternak lele',30000,80000,'buat aja seperti itu',0,'2021-01-25','1',NULL),(7,'INVOICE-190','1','ashri29410','Jl. Pringgan Pasar 2 Helvetia','Sumatera Utara','Deli Serdang','Sunggal','20124','',30000,30250,'',0,'2021-01-25','1','image_2021012508313719376.jpg'),(8,'INVOICE-493','3','ashri29410','Jl. Pringgan Pasar 2 Helvetia','Sumatera Utara','Deli Serdang','Sunggal','20124','',30000,130000,'fotonya ambil di ig @koko',0,'2021-01-28','2',NULL),(10,'INVOICE-609',NULL,'ashri29410','Jl. Pringgan Pasar 2 Helvetia','Sumatera Utara','Deli Serdang','Sunggal','20124','',10000,20000,'Print kertas A4 sebanyak 100 lembar dicetak dengan tinta Hitam',1,'2021-02-11',NULL,'file_2021021123342812069.docx'),(11,'INVOICE-1334','3','ashri29410','Jl. Pringgan Pasar 2 Helvetia','Sumatera Utara','Medan',NULL,'20124','dsaasdds',7000,57000,'dasasdad',0,'2021-02-20','1',NULL);
/*!40000 ALTER TABLE `product_order` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `setting_admin`
--

DROP TABLE IF EXISTS `setting_admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `setting_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_setting` varchar(30) DEFAULT NULL,
  `value` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `setting_admin`
--

LOCK TABLES `setting_admin` WRITE;
/*!40000 ALTER TABLE `setting_admin` DISABLE KEYS */;
/*!40000 ALTER TABLE `setting_admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_category`
--

DROP TABLE IF EXISTS `sub_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_category` int(11) DEFAULT NULL,
  `nama_subcategory` varchar(30) DEFAULT NULL,
  `permalink` text DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_category`
--

LOCK TABLES `sub_category` WRITE;
/*!40000 ALTER TABLE `sub_category` DISABLE KEYS */;
INSERT INTO `sub_category` VALUES (1,3,'photo books','photo-books'),(2,3,'photo print','photo-print'),(3,3,'photo karikatur','photo-karikatur'),(4,3,'foto mozaik','foto-mozaik'),(5,3,'foto siluet','foto-siluet'),(6,3,'foto wisuda','foto-wisuda'),(7,4,'kartu ucapan','kartu-ucapan'),(8,4,'seminar','seminar'),(9,4,'memories','memories'),(10,4,'pernikahan','pernikahan'),(11,5,'kop surat','kop-surat'),(12,5,'amplop','amplop'),(13,5,'lanyard','lanyard'),(14,5,'maps folder','maps-folder'),(15,5,'HVS','HVS'),(16,5,'alat kantor','alat-kantor'),(17,6,'kemasan makanan','kemasan-makanan'),(18,6,'food tray','food-tray'),(19,6,'paper cup','paper-cup'),(20,7,'brosur','brosur'),(21,7,'banner','banner'),(22,7,'flayer','flayer');
/*!40000 ALTER TABLE `sub_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_ukuran`
--

DROP TABLE IF EXISTS `type_ukuran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_ukuran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ukuran_size` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_ukuran`
--

LOCK TABLES `type_ukuran` WRITE;
/*!40000 ALTER TABLE `type_ukuran` DISABLE KEYS */;
INSERT INTO `type_ukuran` VALUES (1,'normal'),(2,'normal'),(3,'besar');
/*!40000 ALTER TABLE `type_ukuran` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_member`
--

DROP TABLE IF EXISTS `user_member`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` text DEFAULT NULL,
  `username` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `foto_profile` text DEFAULT NULL,
  `no_handphone` varchar(13) DEFAULT NULL,
  `register_date` datetime DEFAULT NULL ON UPDATE current_timestamp(),
  `status_user` int(11) DEFAULT NULL,
  `nama_user` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_member`
--

LOCK TABLES `user_member` WRITE;
/*!40000 ALTER TABLE `user_member` DISABLE KEYS */;
INSERT INTO `user_member` VALUES (1,'ashri.prastiko86@gmail.com','ashri29410','237ff912e5c9f5693391bc492e280e53',NULL,'081270459625','2021-01-28 20:21:25',1,'ashri prastiko juned'),(2,'harifana.ulfakh@gmail.com','Hari68707','5841a22029ca103fbc087f6329215b26',NULL,'081264125129','2021-01-16 20:06:04',1,'Hari Fana Riki Ulfakh Sinaga');
/*!40000 ALTER TABLE `user_member` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'forsilaa_creative'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-03 11:37:58

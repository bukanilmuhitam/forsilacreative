<?php namespace App\Models;

use CodeIgniter\Model;

class CartModel extends Model
{

    protected $table    = 'cart_produk';
    protected $primaryKey = 'id';

    protected $allowedFields = ['id_product' , 'username' , 'qty'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';


    function getData($id= '' , $page = ''){
        $pager = \Config\Services::pager('');
        if($page == ''){
            if($id == ''){
                return $this->findAll();
            }else{
                return $this->where('id' , $id)->first();
            }
        }else{
            $data = [
                'items' => $this->orderBy('id' , 'DESC')->paginate($page , 'bootstrap'),
                'pager' => $this->pager,
            ];

            return $data;
        }
    }
    
    function getDataByUsername($username){
        return $this->where('username' , $username)->first();
    }

    function check_cart($username = ''){
        return $this->where('username' , $username)->findAll();
    }

    function deleteByIdproduct($id){
        $query = $this->where('id_product', $id)->delete();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }
    function deleteByUsername($id){
        $query = $this->where('username', $id)->delete();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }
    
    function joinOrder(){

        $db      = \Config\Database::connect();
        $builder = $db->table('projects')
        ->select('*')
        ->join('orders' , 'orders.id = projects.id_order') 
        ->get();
        return $builder;


    }

    function saveData($data){
        $query = $this->insert($data);
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

    function updatedata($id , $data){

       $query = $this->update($id , $data);
       if($query){
           return 'ok';
       }else{
           return 'error';
       }

    }

    function updateByIdproduct($id , $data){
        $query = $this->where('id_product' , $id)->set($data)->update();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

    function deletedata($id){
        $query = $this->where('id', $id)->delete();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

}
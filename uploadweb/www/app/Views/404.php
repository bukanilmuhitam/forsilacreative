<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>
<!-- Jumbotron -->
<section class="mt-14 mb-0 flex justify-center">
    <img class="hidden md:block" src="<?=base_url('upload/banner/404.jpg')?>" style="height: 400px;" alt="400">
    <img class="block md:hidden" src="<?=base_url('upload/banner/404.jpg')?>" style="height: 200px;" alt="400">
</section>
<section>
<h3 class="text-center font-semibold"><?=$message?></h3>
</section>

<?= $this->endSection()?>


<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
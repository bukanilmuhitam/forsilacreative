<footer class="mt-10 bg-black text-white hidden md:block">
    <div class="max-w-screen-sm md:max-w-screen-md lg:max-w-screen-lg mx-4 md:mx-12 lg:mx-auto">
        <div class="p-6">
            <div class="my-4 flex">
                <div>
                    <h3 class="uppercase font-bold">Forsila Creative</h3>
                    <p>Jl. Gatot Subroto , no. 34.</p>
                    <p>Develope By Ulfa</p>
                    <div class="mt-24 flex space-x-2 text-sm">
                        <a href="/">Kebijakan Privasi</a>
                        <a href="/">Syarat Dan Ketentuan</a>
                    </div>
                </div>
                <div class="ml-auto">
                    <h3 class="font-bold mb-6">INFORMATION</h3>
                    <form action="/contact" method="post" class="flex flex-col space-y-4">
                        <input type="text" name="email"
                            class="border border-white rounded py-1 px-4 bg-transparent focus:outline-none"
                            placeholder="Email">
                        <input type="text" name="message"
                            class="border border-white rounded py-1 px-4 bg-transparent focus:outline-none"
                            placeholder="Message">
                        <button class="py-1 px-4 bg-white rounded text-black">Kirim</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</footer>
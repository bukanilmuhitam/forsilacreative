<?php 
$uri = $_SERVER['REQUEST_URI'];
$getUri = str_replace('/' , '' , $uri);
?>
<aside class="bg-white border border-gray-200 h-screen overflow-hidden side-menu w-0 lg:w-72 active-side">

    <!-- Profile -->
    <div class="w-full my-4 mx-4 flex justify-start space-x-4" id="profile">
        <div class="w-14 h-14 rounded-full border border-gray-100 overflow-hidden flex items-center">
            <img src="<?=base_url('admin/assets/images/user.svg')?>" class="w-full rounded-full" alt="image-profile">
        </div>
        <div class="flex flex-col">
            <h3 class="font-semibold text-lg"><?= session()->get('user_name')?></h3>
            <!-- <div class="flex items-center ">
                <span class="w-2 h-2 bg-green-400 rounded-full flex items-center mr-1"></span>
                <p class="flex items-center text-gray-300 font-light">online</p>
            </div> -->
        </div>
    </div>
    <!-- End Profile -->
    <!-- Navigation -->
    <div class="menu-side">
        <ul class="flex flex-col space-y-1 mb-40">
            <li class="title-menu mx-4">MAIN NAVIGATION</li>
            <li class="menu-content  
                <?php 
                    if(in_array($getUri , array('administrator'))){
                        echo 'text-active';
                    }else{
                        echo '';
                    }
                ?>">
                <a href="/administrator" class="flex spcae-x-3">
                    <div class="flex items-center space-x-2">
                        <i class="fas fa-tachometer-alt"></i><span class="font-normal text-sm">Dashboard</span>
                    </div>
                </a>
            </li>
            <li class="menu-content">
                <button type="button" class="flex justify-between w-full focus:outline-none menubtn-lv1">
                    <div class="flex items-center space-x-2">
                        <i class="fas fa-users"></i><span class="font-normal text-sm">Members</span>
                    </div>
                    <div class="flex items-center">
                        <i class="fa fa-angle-down fa-sm ic-toogle"></i>
                    </div>
                </button>
                <ul class="mt-3 flex flex-col space-y-3 treemenu 
                    <?php 
                            if(in_array($getUri , array('listmember' , 'bannedmember'))){
                                echo 'active';
                            }else{
                                echo '';
                            }                          
                    ?>
                ">
                    <li class="ml-1 <?=$getUri == 'listmember' ? 'text-active' : ''?>">
                        <a href="/listmember" class="flex items-center space-x-2">
                            <i class="far fa-circle text-gray-200"></i><span class="font-normal text-sm">List
                                member</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-content">
                <button type="button" class="flex justify-between w-full focus:outline-none menubtn-lv1">
                    <div class="flex items-center space-x-2">
                        <i class="fas fa-box"></i><span class="font-normal text-sm">Products</span>
                    </div>
                    <div class="flex items-center">
                        <i class="fa fa-angle-down fa-sm ic-toogle"></i>
                    </div>
                </button>
                <ul class="mt-3 flex flex-col space-y-3 treemenu
                    <?php 
                            if(in_array($getUri , array('category' , 'subcategory' , 'listproduk'))){
                                echo 'active';
                            }else{
                                echo '';
                            }                          
                    ?>
                ">
                    <li class="ml-1 <?=$getUri == 'category' ? 'text-active' : ''?>">
                        <a href="/category" class="flex items-center space-x-2">
                            <i class="far fa-circle text-gray-200"></i><span class="font-normal text-sm">Kategori
                                produk</span>
                        </a>
                    </li>
                    <li class="ml-1 <?=$getUri == 'subcategory' ? 'text-active' : ''?>">
                        <a href="/subcategory" class="flex items-center space-x-2">
                            <i class="far fa-circle text-gray-200"></i><span class="font-normal text-sm">Sub Kategori
                                produk</span>
                        </a>
                    </li>
                    <li class="ml-1 <?=$getUri == 'listproduk' ? 'text-active' : ''?>">
                        <a href="/listproduk" class="flex items-center space-x-2">
                            <i class="far fa-circle text-gray-200"></i><span class="font-normal text-sm">List
                                produk</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-content">
                <button type="button" class="flex justify-between w-full focus:outline-none menubtn-lv1">
                    <div class="flex items-center space-x-2">
                        <i class="fas fa-shopping-cart"></i><span class="font-normal text-sm">Order</span>
                    </div>
                    <div class="flex items-center">
                        <i class="fa fa-angle-down fa-sm ic-toogle"></i>
                    </div>
                </button>
                <ul class="mt-3 flex flex-col space-y-3 treemenu
                    <?php 
                            if(in_array($getUri , array('listorder' , 'laporanpenjualan' , 'reportorder'))){
                                echo 'active';
                            }else{
                                echo '';
                            }                          
                    ?>
                ">
                    <li class="ml-1 <?=$getUri == 'listorder' ? 'text-active' : ''?>">
                        <a href="/listorder" class="flex items-center space-x-2">
                            <i class="far fa-circle text-gray-200"></i><span class="font-normal text-sm">List
                                Order</span>
                        </a>
                    </li>
                    <li class="ml-1 <?=$getUri == 'reportorder' ? 'text-active' : ''?>">
                        <a href="/reportorder" class="flex items-center space-x-2">
                            <i class="far fa-circle text-gray-200"></i><span class="font-normal text-sm">Report Order</span>
                        </a>
                    </li>
    
                </ul>
            </li>
            <li class="menu-content  
                <?php 
                    if(in_array($getUri , array('adminsetting'))){
                        echo 'text-active';
                    }else{
                        echo '';
                    }
                ?>">
                <a href="/adminsetting" class="flex spcae-x-3">
                    <div class="flex items-center space-x-2">
                        <i class="fas fa-cogs"></i><span class="font-normal text-sm">Admin Setting</span>
                    </div>
                </a>
            </li>
        </ul>
        <!-- <p>dad</p> -->
    </div>
    <!-- End Naviagtion -->
</aside>
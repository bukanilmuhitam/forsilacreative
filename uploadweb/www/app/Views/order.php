<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>
<!-- This New Products -->
<section class="mt-6 lg:mt-8">
    <?php
        if(!empty(session()->getFlashdata('msg'))){ ?>


    <div class="my-4 w-full py-2 px-6 border border-green-500 bg-green-400 text-white rounded-md">
        <?php echo session()->getFlashdata('msg');?>
    </div>

    <?php } ?>
    <div class="flex flex-col space-y-6">
        <div class="w-full border">
            <div class="border-b p-4">
                <h3 class="text-lg font-semibold">Order Product</h3>
            </div>
            <div class="p-4">
                <?php 

                    $total = 0;
                    $id_product = '';
                    $qtyperproduct = '';
                    foreach($cart as $ct){
                        foreach($products as $product){
                            if($ct['id_product'] == $product['id']){
                                $subtotal = $ct['qty'] * $product['harga_default_pcs'];
                                $total += $subtotal;
                                $id_product .= "" . $product['id'] . ",";
                                $qtyperproduct .= "" . $ct['qty'] . ",";
                ?>
                <div class="w-full flex space-x-6 mb-6">
                    <div class="w-32">
                        <div class="w-32 h-32 overflow-hidden">
                            <img src="<?=base_url()?>/upload/product/<?=$product['gambar_produk'];?>"
                                alt="<?=$product['nama_produk'];?>">
                        </div>
                    </div>
                    <div class="flex-1">
                        <form action="/produk/hapuscart" method="POST">
                            <div class="flex justify-between">
                                <h3 class="capitalize font-semibold text-2xl mb-2"><?=$product['nama_produk'];?></h3>

                                <input type="hidden" name="idproduct" value="<?=$product['id'];?>">
                                <button type="submit" class="focus:outline-none"><i
                                        class="text-red-500 fa fa-trash"></i></button>

                            </div>
                        </form>

                        <p>Rp. <?=$product['harga_default_pcs'];?></p>
                        <div class="flex items-center space-x-4">
                            <span class="font-bold text-sm">Quantity : </span>
                            <form action="/produk/updatequantity" method="POST" class="flex items-center space-x-2">
                                <input type="hidden" name="idproduct" value="<?=$product['id'];?>">
                                <input type="number" name="qty" value="<?=$ct['qty'];?>"
                                    class="py-1 px-4 border rounded focus:outline-none text-sm">
                                <button type="submit"
                                    class="text-sm py-1 px-4 focus:outline-none bg-blue-600 text-white rounded-md">Update</button>
                            </form>
                        </div>
                        <p><span class="font-bold text-sm">Sub Total : Rp.
                                <?=number_format($subtotal , 0 , '.' , '.');?></span></p>
                    </div>
                </div>


                <?php }}}
                    $id_product = rtrim($id_product , ',');
                    $qtyperproduct = rtrim($qtyperproduct ,',');
                ?>
            </div>

            <div class="p-4 border-t">
                <div class="flex">
                    <h3 class="font-bold text-lg">TOTAL :</h3>
                    <p class="ml-auto">Rp. <?=number_format($total , 0 , '.' , '.')?></p>
                </div>
            </div>
        </div>
        <script type="text/javascript">
        window.totalpembayaran = <?php
          echo json_encode([
              'total' => $total,
          ]);
        ?>
        </script>
        <form action="/order/saveorder" method="post" enctype="multipart/form-data">
            <input type="hidden" name="id_product" value="<?=$id_product?>">
            <input type="hidden" name="qtyperproduct" value="<?=$qtyperproduct?>">
            <div class="w-full border mb-6">
                <div class="border-b p-4">
                    <h3 class="text-lg font-semibold">Shipping</h3>
                </div>
                <div class="p-4">
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Alamat pengiriman</label>
                        <input type="text" name="alamat_pengiriman" name="Alamat Pengantaran"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('alamat_pengiriman'))){ echo "border-red-700";}?>">
                        <?php 
                            if(!empty($validation->getError('alamat_pengiriman'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('alamat_pengiriman')?></small>
                        <?php }?>
                    </div>
                    <!-- <div class="mb-6 flex flex-col">
                    <label for="" class="text-gray-400">Provinsi</label>
                    <input type="text" name="provinsi" placeholder="Masukan provinsi" class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                    <?php 
                            if(!empty($validation->getError('provinsi'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('provinsi')?></small>
                    <?php }?>
                </div> -->
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Provinsi</label>
                        <select v-model="provinsi" name="provinsi"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700"
                            @change="getKabupaten">
                            <option value="">Provinsi</option>
                            <?php 
                                foreach($provinsi as $value){
                            ?>
                            <option value="<?=$value->province_id."-".$value->province?>"><?=$value->province?></option>
                            <?php }?>
                        </select>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Kabupaten</label>
                        <select name="kabupaten" v-if="kabupaten.length != 0" v-model="kab_kota"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700"
                            @change="showOngkir">
                            <option value="">Kabupaten</option>
                            <option v-for="value in kabupaten" :value="value.city_id+'-'+value.city_name">
                                {{ value.city_name }}</option>
                        </select>
                        <select name="kabupaten" v-else
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700"
                            disabled>
                            <option value="">Kabupaten</option>
                        </select>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Ongkos kirim</label>
                        <select name="ongkir" v-if="ongkir.length != 0" v-model="ongkos_kirim"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700">
                            <option value="">Ongkir</option>
                            <option v-for="value in ongkir" :value="value.cost[0].value">JNE - {{ value.service }} (Rp.
                                {{  formatPrice(value.cost[0].value)}}) estimasi {{ value.cost[0].etd }} hari</option>
                        </select>
                        <select name="ongkir" v-else
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700"
                            disabled>
                            <option value="">Ongkir</option>
                        </select>
                        <small v-if="loadCekongkir === true">Loading cek ongkir ....</small>
                    </div>
                    <!-- <div class="mb-6 flex flex-col">
                    <label for="" class="text-gray-400">Kabupaten</label>
                    <input type="text" name="kabupaten" placeholder="Masukan Kabupaten" class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                    <?php 
                            if(!empty($validation->getError('kabupaten'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('kabupaten')?></small>
                    <?php }?>
                </div> -->
                    <!-- <div class="mb-6 flex flex-col">
                    <label for="" class="text-gray-400">Kecamatan</label>
                    <input type="text" name="kecamatan" placeholder="Masukan kecamatan" class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                    <?php 
                            if(!empty($validation->getError('kecamatan'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('kecamatan')?></small>
                    <?php }?>
                </div> -->
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Kode Pos</label>
                        <input type="text" name="kodepos" placeholder="Masukan kodepos"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                        <?php 
                            if(!empty($validation->getError('kodepos'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('kodepos')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Detail Alamat Tambahan (Optional)</label>
                        <small class="mb-2 text-gray-400">*misalnya pagarnya warna merah, samping kantor desan atau
                            lainnya</small>
                        <textarea name="detail_alamat"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700"></textarea>
                    </div>
                </div>
            </div>
            <div class="w-full border mb-6">
                <div class="border-b p-4">
                    <h3 class="text-lg font-semibold">Keterangan design</h3>
                    <small class="text-gray-400">*jabarkan design yang anda inginkan disini</small>
                </div>
                <div class="p-4">
                    <div class="mb-6 flex flex-col">
                        <textarea name="catatan_tambahan"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700"></textarea>
                    </div>
                </div>
            </div>
            <div class="w-full border mb-6">
                <div class="border-b p-4">
                    <h3 class="text-lg font-semibold">Kirim File Design</h3>
                    <small class="text-gray-400">*lampirkan file design anda disini</small>
                </div>
                <div class="p-4">
                    <div class="mb-6 flex flex-col">
                        <input type="file" name="gambar_design"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700">
                    </div>
                </div>
            </div>
            <div class="w-full border mb-6">
                <div class="border-b p-4">
                    <h3 class="text-lg font-semibold">PEMBAYARAN</h3>
                </div>
                <div class="p-4">
                    <div class="flex items-center">
                        <div>
                            <table class="">
                                <tbody>
                                    <tr>
                                        <td class="font-bold uppercase">TOTAL BELANJA</td>
                                        <td>:</td>
                                        <td>Rp. <?=number_format($total , 0 , '.' , '.')?></td>
                                    </tr>
                                    <tr v-if="ongkos_kirim != ''">
                                        <td class="font-bold uppercase">Biaya Pengiriman</td>
                                        <td>:</td>
                                        <td>Rp. {{ formatPrice(ongkos_kirim) }}</td>
                                    </tr>
                                    <tr>
                                        <td class="font-bold uppercase">TOTAL PEMBAYARAN</td>
                                        <td>:</td>
                                        <td>Rp. {{ jumlahTotal() }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="ml-auto">
                            <button type="submit"
                                class="py-1 px-2 bg-red-700 text-white rounded">Pemesanan</button>
                        </div>
                    </div>
                </div>
            </div>

        </form>
    </div>

</section>
<!-- New Products -->



<?= $this->endSection()?>

<?= $this->section('footer') ?>

<!-- 
<footer class="bg-white border p-6 fixed w-full bottom-0 shadow z-40">
    <div class="max-w-screen-sm md:max-w-screen-md lg:max-w-screen-lg mx-4 md:mx-12 lg:mx-auto">
        <div class="flex items-center">
            <div>
                <table class="">
                    <tbody>
                        <tr>
                            <td class="font-bold uppercase">TOTAL BELANJA</td>
                            <td>:</td>
                            <td>Rp. <?=number_format($total , 0 , '.' , '.')?></td>
                        </tr>
                        <tr v-if="ongkos_kirim != ''">
                            <td class="font-bold uppercase">Biaya Pengiriman</td>
                            <td>:</td>
                            <td>Rp. {{ formatPrice(ongkos_kirim) }}</td>
                        </tr>
                        <tr>
                            <td class="font-bold uppercase">TOTAL PEMBAYARAN</td>
                            <td>:</td>
                            <td>Rp. {{ jumlahTotal() }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="ml-auto">
                <button type="submit" v-if="ongkos_kirim != 0"
                    class="py-1 px-2 bg-red-700 text-white rounded">Pemesanan</button>
                </form>
            </div>
        </div>
    </div>
</footer> -->

<?= $this->endSection()?>
<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>

<?php
if(!empty(session()->getFlashdata('msg'))){ ?>


<div class="my-4 w-full py-2 px-6 border border-green-500 bg-green-400 text-white rounded-md">
    <?php echo session()->getFlashdata('msg');?>
</div>

<?php } ?>


<!-- This New Products -->
<section class="my-6 lg:my-16">
    <div class="flex space-x-6">
        <?php 
            echo view('user/profile/left_menu.php');
        ?>
        <div class="flex-1">
            <!-- Breadchumb -->
            <div class="flex mt-2 space-x-4">
                <a href="/profile" class="text-blue-400">Transaksi</a>
                <p class="text-gray-400"><?=$number_invoice?></p>
            </div>
            <div class="mt-4">
                <?php 

                    if($detail['status_order'] == 0){
                        $status = 'Belum diproses';
                        $color = 'text-black';
                        $statbutton = 'proses';
                    }else if($detail['status_order'] == 1){
                        $status = 'Barang diproses';
                        $color = 'text-yellow-600'; 
                        $statbutton = 'shipping';   
                    }else if($detail['status_order'] == 2){
                        $status = 'Barang diantar';
                        $color = 'text-blue-600';    
                        $statbutton = 'complete';
                    }else if($detail['status_order'] == 3){
                        $status = 'Order Complete';
                        $color = 'text-green-600';    
                        $statbutton = 'batal';
                    }else{
                        $status = 'Pesanan Batal';
                        $color = 'text-red-600';    
                        $statbutton = 'proses';
                    }
                                    
                ?>
                <h6 class="<?=$color?>">
                                    Status Order : <span class="font-semibold"><?=$status?></span>
                </h6>
            </div>
            <div class="mt-4">
                <h3 class="text-lg font-semibold">Informasi Order</h3>
                <table class=" mb-6">
                    <tr>
                        <td>Username</td>
                        <td>:</td>
                        <td><?=$detail['username'];?></td>
                    </tr>
                    <tr>
                        <td>Alamat Pengiriman</td>
                        <td>:</td>
                        <td><?=$detail['alamat_pengiriman'];?></td>
                    </tr>
                    <tr>
                        <td>Provinsi</td>
                        <td>:</td>
                        <td><?=$detail['provinsi'];?></td>
                    </tr>
                    <tr>
                        <td>Kabupaten</td>
                        <td>:</td>
                        <td><?=$detail['kabupaten'];?></td>
                    </tr>
                    <tr>
                        <td>Kecamatan</td>
                        <td>:</td>
                        <td><?=$detail['kecamatan'];?></td>
                    </tr>
                    <tr>
                        <td>Kode Pos</td>
                        <td>:</td>
                        <td><?=$detail['kode_pos'];?></td>
                    </tr>
                </table>

                <h3 class="text-lg font-semibold">Informasi Tambahan</h3>
                <table class=" mb-6">
                    <tr>
                        <td>Detail Pengiriman</td>
                        <td>:</td>
                        <td><?=!empty($detail['keterangan_tambahan_pengiriman']) ? $detail['keterangan_tambahan_pengiriman'] : '-';?>
                        </td>
                    </tr>
                    <tr>
                        <td>Catatan Tambahan</td>
                        <td>:</td>
                        <td><?=!empty($detail['catatan_tambahan']) ? $detail['catatan_tambahan'] : '-';?></td>
                    </tr>
                </table>

                <h3 class="text-lg font-semibold">Detail Produk</h3>
                <div class="mt-4 mb-6">
                    <?php       
                        $total = 0;
                        $explode = explode(',' ,  $detail['product_id']);
                        $qty = explode(',' , $detail['qty_perproduct'] );
                        $nama_product = '';
                        foreach ($products as $product) {
                            for($i= 0 ; $i< count($explode); $i++){
                              if($product['id'] == $explode[$i]){

                                $subtotal = $qty[$i] * $product['harga_default_pcs'];
                                $total += $subtotal;
                                   
                       
            ?>
                    <div class="w-full flex space-x-6 mb-6">
                        <div class="w-32">
                            <div class="w-32 h-32 overflow-hidden">
                                <img src="<?=base_url()?>/upload/product/<?=$product['gambar_produk'];?>"
                                    alt="<?=$product['nama_produk'];?>">
                            </div>
                        </div>
                        <div class="flex-1">
                            <h3 class="capitalize font-semibold text-2xl mb-2"><?=$product['nama_produk'];?></h3>
                            <p>Rp. <?=$product['harga_default_pcs'];?></p>
                            <div class="flex items-center space-x-4">
                                <span class="font-bold text-sm">Quantity : </span>
                                <?=$qty[$i]?>
                            </div>
                            <p><span class="font-bold text-sm">Sub Total : Rp.
                                    <?=number_format($subtotal , 0 , '.' , '.');?></span></p>
                        </div>
                    </div>

                    <?php 
            
                        }
                    }
                }
            ?>
                </div>
                <h3 class="text-lg font-semibold">Total Pembayaran</h3>
                <table class="mb-6">
                    <tr>
                        <td>Total Belanja</td>
                        <td>:</td>
                        <td>Rp. <?=number_format($total , 0 , '.' , '.')?></td>
                    </tr>
                    <tr>
                        <td>Biaya Shipping</td>
                        <td>:</td>
                        <td>Rp. <?=number_format($detail['tshipping'] , 0 , '.' , '.')?></td>
                    </tr>
                    <tr>
                        <td>Total Pembayaran</td>
                        <td>:</td>
                        <td>Rp.
                            <?php $tpyment = $total + $detail['tshipping']; echo number_format($tpyment , 0 , '.' , '.')?>
                        </td>
                    </tr>
                </table>
            </div>

        </div>
    </div>
</section>
<!-- New Products -->

<?= $this->endSection()?>


<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
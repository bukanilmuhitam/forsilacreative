<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>
<!-- This New Products -->
<section class="my-6 lg:my-16">
    <div class="flex space-x-6">
        <div class="hidden md:block w-64">
            <div class="p-4 border">
                <h3 class="font-bold text-gray-400 text-md"><?=ucwords($view_by)?> lainnya :</h3>
            </div>
            <?php 
            foreach($similiar_categories as $similiar_categories){
        ?>
            <div class="p-4 border">
                <a href="<?=$similiar_categories['permalink'];?>"><?=$similiar_categories['nama_subcategory'];?></a>
            </div>
            <?php }?>
        </div>
        <div class="flex-1">
            <p class="italic text-sm">Menampilkan <?=$countProduct?> product dari <?=ucwords($view_by)?> <span
                    class="font-bold">"<?=$nama_subcategory?>"</span></p>
            <div class="mt-4">

                <!-- Show New Product -->
                <div class='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4'>
                   <?php echo view('card');?>
                </div>
                <!-- End Show -->

            </div>
        </div>
    </div>
</section>
<!-- New Products -->

<?= $this->endSection()?>


<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
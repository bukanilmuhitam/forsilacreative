<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>

<?php
if(!empty(session()->getFlashdata('msg'))){ ?>


<div class="my-4 w-full py-2 px-6 border border-green-500 bg-green-400 text-white rounded-md">
    <?php echo session()->getFlashdata('msg');?>
</div>

<?php } ?>


<!-- This New Products -->
<section class="my-6 lg:my-16">
    <div class="flex space-x-6">
        <?php 
            echo view('user/profile/left_menu.php');
        ?>
        <div class="flex-1">
            <h3 class="font-semibold uppercase">Transaksi</h3>
            <div class="mt-4">

                <div class="flex border rounded space-x-6">
                    <button type="button" data-id="tab-1" class="tablink py-2 px-4 focus:outline-none active-tab">Belum
                        proses (<?=$jumlahbelumproses?>)</button>
                    <button type="button" data-id="tab-2" class="tablink focus:outline-none py-2 px-4">Diproses
                        (<?=$jumlahproses?>)</button>
                    <button type="button" data-id="tab-3" class="tablink focus:outline-none py-2 px-4">Dikirim
                        (<?=$jumlahshipping?>)</button>
                    <button type="button" data-id="tab-4" class="tablink focus:outline-none py-2 px-4">Diterima
                        (<?=$jumlahcomplete?>)</button>
                    <button type="button" data-id="tab-5" class="tablink focus:outline-none py-2 px-4">Dibatalkan
                        (<?=$jumlahbatal?>)</button>
                </div>
                <div id="tab-1" class="tabs border-r border-l border-b rounded p-4 active">
                    <?php 
                    if($jumlahbelumproses > 0){
                   ?>
                        <?php 
                            $no = 1;
                            foreach($belumproses as $rowbelumproses){
                                if($rowbelumproses['status_order'] == 0){
                                    $status = 'Belum diproses';
                                    $color = 'text-black';
                                    $statbutton = 'proses';
                                 }else if($rowbelumproses['status_order'] == 1){
                                     $status = 'Barang diproses';
                                     $color = 'text-yellow-600'; 
                                     $statbutton = 'shipping';   
                                 }else if($rowbelumproses['status_order'] == 2){
                                     $status = 'Barang diantar';
                                     $color = 'text-blue-600';    
                                     $statbutton = 'complete';
                                 }else if($rowbelumproses['status_order'] == 3){
                                     $status = 'Order Complete';
                                     $color = 'text-green-600';    
                                     $statbutton = 'batal';
                                 }else{
                                     $status = 'Pesanan Batal';
                                     $color = 'text-red-600';    
                                     $statbutton = 'proses';
                                 }
                        ?>
                                <div class="p-4 border mb-4">
                                    <div class="flex">
                                        <div>
                                            <a href="/profile/detailtransaksi/<?=$rowbelumproses['number_invoice'];?>"
                                                class="text-lg font-semibold underline text-blue-600">#<?=$no++?>.
                                                <?=$rowbelumproses['number_invoice'];?></a>
                                            <div class="mt-4">
                                                <p>Date Order : <?=$rowbelumproses['date_order']?></p>
                                                <p>Username : <?=$rowbelumproses['username'];?></p>
                                            </div>
                                            <h6 class="<?=$color?>">
                                                Status Order : <span class="font-semibold"><?=$status?></span>
                                            </h6>
                                        </div>
                                        <div class="flex items-center ml-auto">
                                            <p class="text-2xl font-semibold">Rp. <?=number_format($rowbelumproses['tpayment'] , 0 , '.' , '.');?></p>
                                        </div>
                                    </div>
                                </div>
                         <?php }?>
                    <?php }else{?>
                    <div class="text-center text-gray-500 mt-8">
                        <i class="fa fa-box-open fa-10x"></i>
                        <h3 class="text-center">Tidak ada barang yang belum diproses</h3>
                    </div>
                    <?php }?>
                </div>
                <div id="tab-2" class="tabs border-r border-l border-b rounded p-4 ">
                    <?php 
                    if($jumlahproses > 0){
                   ?>
                          <?php 
                            $no = 1;
                            foreach($proses as $rowproses){
                                if($rowproses['status_order'] == 0){
                                    $status = 'Belum diproses';
                                    $color = 'text-black';
                                    $statbutton = 'proses';
                                 }else if($rowproses['status_order'] == 1){
                                     $status = 'Barang diproses';
                                     $color = 'text-yellow-600'; 
                                     $statbutton = 'shipping';   
                                 }else if($rowproses['status_order'] == 2){
                                     $status = 'Barang diantar';
                                     $color = 'text-blue-600';    
                                     $statbutton = 'complete';
                                 }else if($rowproses['status_order'] == 3){
                                     $status = 'Order Complete';
                                     $color = 'text-green-600';    
                                     $statbutton = 'batal';
                                 }else{
                                     $status = 'Pesanan Batal';
                                     $color = 'text-red-600';    
                                     $statbutton = 'proses';
                                 }
                        ?>
                                <div class="p-4 border mb-4">
                                    <div class="flex">
                                        <div>
                                            <a href="/profile/detailtransaksi/<?=$rowproses['number_invoice'];?>"
                                                class="text-lg font-semibold underline text-blue-600">#<?=$no++?>.
                                                <?=$rowproses['number_invoice'];?></a>
                                            <div class="mt-4">
                                                <p>Date Order : <?=$rowproses['date_order']?></p>
                                                <p>Username : <?=$rowproses['username'];?></p>
                                            </div>
                                            <h6 class="<?=$color?>">
                                                Status Order : <span class="font-semibold"><?=$status?></span>
                                            </h6>
                                        </div>
                                        <div class="flex items-center ml-auto">
                                            <p class="text-2xl font-semibold">Rp. <?=number_format($rowproses['tpayment'] , 0 , '.' , '.');?></p>
                                        </div>
                                    </div>
                                </div>
                         <?php }?>
                    <?php }else{?>
                    <div class="text-center text-gray-500 mt-8">
                        <i class="fa fa-box-open fa-10x"></i>
                        <h3 class="text-center">Tidak ada barang yang diproses</h3>
                    </div>
                    <?php }?>
                </div>
                <div id="tab-3" class="tabs border-r border-l border-b rounded p-4 ">
                    <?php 
                    if($jumlahshipping > 0){
                   ?>
                          <?php 
                            $no = 1;
                            foreach($shipping as $rowshipping){
                                if($rowshipping['status_order'] == 0){
                                    $status = 'Belum diproses';
                                    $color = 'text-black';
                                    $statbutton = 'proses';
                                 }else if($rowshipping['status_order'] == 1){
                                     $status = 'Barang diproses';
                                     $color = 'text-yellow-600'; 
                                     $statbutton = 'shipping';   
                                 }else if($rowshipping['status_order'] == 2){
                                     $status = 'Barang diantar';
                                     $color = 'text-blue-600';    
                                     $statbutton = 'complete';
                                 }else if($rowshipping['status_order'] == 3){
                                     $status = 'Order Complete';
                                     $color = 'text-green-600';    
                                     $statbutton = 'batal';
                                 }else{
                                     $status = 'Pesanan Batal';
                                     $color = 'text-red-600';    
                                     $statbutton = 'proses';
                                 }
                        ?>
                                <div class="p-4 border mb-4">
                                    <div class="flex">
                                        <div>
                                            <a href="/profile/detailtransaksi/<?=$rowshipping['number_invoice'];?>"
                                                class="text-lg font-semibold underline text-blue-600">#<?=$no++?>.
                                                <?=$rowshipping['number_invoice'];?></a>
                                            <div class="mt-4">
                                                <p>Date Order : <?=$rowshipping['date_order']?></p>
                                                <p>Username : <?=$rowshipping['username'];?></p>
                                            </div>
                                            <h6 class="<?=$color?>">
                                                Status Order : <span class="font-semibold"><?=$status?></span>
                                            </h6>
                                        </div>
                                        <div class="flex items-center ml-auto">
                                            <p class="text-2xl font-semibold">Rp. <?=number_format($rowshipping['tpayment'] , 0 , '.' , '.');?></p>
                                        </div>
                                    </div>
                                </div>
                         <?php }?>
                    <?php }else{?>
                    <div class="text-center text-gray-500 mt-8">
                        <i class="fa fa-box-open fa-10x"></i>
                        <h3 class="text-center">Tidak ada barang yang dikirim</h3>
                    </div>
                    <?php }?>
                </div>
                <div id="tab-4" class="tabs border-r border-l border-b rounded p-4 ">
                    <?php 
                    if($jumlahcomplete > 0){
                   ?>

                            <?php 
                            $no = 1;
                            foreach($complete as $rowcomplete){
                                if($rowcomplete['status_order'] == 0){
                                    $status = 'Belum diproses';
                                    $color = 'text-black';
                                    $statbutton = 'proses';
                                 }else if($rowcomplete['status_order'] == 1){
                                     $status = 'Barang diproses';
                                     $color = 'text-yellow-600'; 
                                     $statbutton = 'shipping';   
                                 }else if($rowcomplete['status_order'] == 2){
                                     $status = 'Barang diantar';
                                     $color = 'text-blue-600';    
                                     $statbutton = 'complete';
                                 }else if($rowcomplete['status_order'] == 3){
                                     $status = 'Order Complete';
                                     $color = 'text-green-600';    
                                     $statbutton = 'batal';
                                 }else{
                                     $status = 'Pesanan Batal';
                                     $color = 'text-red-600';    
                                     $statbutton = 'proses';
                                 }
                        ?>
                                <div class="p-4 border mb-4">
                                    <div class="flex">
                                        <div>
                                            <a href="/profile/detailtransaksi/<?=$rowcomplete['number_invoice'];?>"
                                                class="text-lg font-semibold underline text-blue-600">#<?=$no++?>.
                                                <?=$rowcomplete['number_invoice'];?></a>
                                            <div class="mt-4">
                                                <p>Date Order : <?=$rowcomplete['date_order']?></p>
                                                <p>Username : <?=$rowcomplete['username'];?></p>
                                            </div>
                                            <h6 class="<?=$color?>">
                                                Status Order : <span class="font-semibold"><?=$status?></span>
                                            </h6>
                                        </div>
                                        <div class="flex items-center ml-auto">
                                            <p class="text-2xl font-semibold">Rp. <?=number_format($rowcomplete['tpayment'] , 0 , '.' , '.');?></p>
                                        </div>
                                    </div>
                                </div>
                         <?php }?>

                    <?php }else{?>
                    <div class="text-center text-gray-500 mt-8">
                        <i class="fa fa-box-open fa-10x"></i>
                        <h3 class="text-center">Tidak ada barang yang diterima</h3>
                    </div>
                    <?php }?>
                </div>
                <div id="tab-5" class="tabs border-r border-l border-b rounded p-4 ">
                    <?php 
                    if($jumlahbatal > 0){
                   ?>

<?php 
                            $no = 1;
                            foreach($batal as $rowbatal){
                                if($rowbatal['status_order'] == 0){
                                    $status = 'Belum diproses';
                                    $color = 'text-black';
                                    $statbutton = 'proses';
                                 }else if($rowbatal['status_order'] == 1){
                                     $status = 'Barang diproses';
                                     $color = 'text-yellow-600'; 
                                     $statbutton = 'shipping';   
                                 }else if($rowbatal['status_order'] == 2){
                                     $status = 'Barang diantar';
                                     $color = 'text-blue-600';    
                                     $statbutton = 'complete';
                                 }else if($rowbatal['status_order'] == 3){
                                     $status = 'Order Complete';
                                     $color = 'text-green-600';    
                                     $statbutton = 'batal';
                                 }else{
                                     $status = 'Pesanan Batal';
                                     $color = 'text-red-600';    
                                     $statbutton = 'proses';
                                 }
                        ?>
                                <div class="p-4 border mb-4">
                                    <div class="flex">
                                        <div>
                                            <a href="/profile/detailtransaksi/<?=$rowbatal['number_invoice'];?>"
                                                class="text-lg font-semibold underline text-blue-600">#<?=$no++?>.
                                                <?=$rowbatal['number_invoice'];?></a>
                                            <div class="mt-4">
                                                <p>Date Order : <?=$rowbatal['date_order']?></p>
                                                <p>Username : <?=$rowbatal['username'];?></p>
                                            </div>
                                            <h6 class="<?=$color?>">
                                                Status Order : <span class="font-semibold"><?=$status?></span>
                                            </h6>
                                        </div>
                                        <div class="flex items-center ml-auto">
                                            <p class="text-2xl font-semibold">Rp. <?=number_format($rowbatal['tpayment'] , 0 , '.' , '.');?></p>
                                        </div>
                                    </div>
                                </div>
                         <?php }?>
                        
                    <?php }else{?>
                    <div class="text-center text-gray-500 mt-8">
                        <i class="fa fa-box-open fa-10x"></i>
                        <h3 class="text-center">Tidak ada barang yang dibatalkan</h3>
                    </div>
                    <?php }?>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- New Products -->

<?= $this->endSection()?>


<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
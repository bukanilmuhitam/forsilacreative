<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Forsila Creative Finish Order</title>
    <!-- Tailwind CSS -->
    <link rel="stylesheet" href="<?=base_url('admin/assets/css/tailwind.css')?>">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?=base_url('admin/assets/css/main.css')?>">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
        crossorigin="anonymous" />
</head>

<body class="bg-gray-200">

   <div class="w-full lg:w-3/5 bg-white  mx-4 lg:mx-auto mt-24">
        <div class="p-6">
            <div class="mb-8">
                <p class="text-gray-400 text-xs">FORSILA CREATIVE</p>
                <h3 class="font-bold text-lg">ORDER COMPLETE</h3>
            </div>
            <div>
                <p>Terimakasih karena telah memasan produk kami.</p>
            </div>
            <div class="my-6">
                <div class="p-6 border-b border-t">
                    <h1 class="font-bold text-2xl">TOTAL PEMBAYARAN : <span class="font-normal">Rp. <?php $totalbayar = session()->get('totalBayar'); echo number_format($totalbayar , 0 , '.' , '.')?></span></h1>
                </div>
            </div>
            <div>
                <p>Kamu bisa melakukan pembayaran lewat transfer bank. melalui rekening dibawah ini :</p>
            </div>
            <h3 class="my-6 font-semibold">BANK TESTING 1809-0909-2987</h3>
            <p>Jika kamu sudah transfer kamu dapat kirim bukti dengan membalas email masuk setelah melakukan orderan.</p>
            <p>Terimakasih telah memasan</p>
            <p>Salam Hangat</p>
            <p class="mt-8">Forsila Team</p>

            <div class="my-8">
            <a href="/" class="btn btn-primary">Mau Belanja Lagi</a>
            </div>
        </div>

        
   </div>


</body>

</html>
<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>
<!-- This New Products -->
<section class="my-6 lg:my-16">
   <div class="flex flex-col lg:flex-row space-x-8">
        <div class="w-full lg:w-2/5 overflow-hidden mb-6">
            <img src="<?=base_url()?>/upload/product/<?=$product['gambar_produk'];?>" alt="<?=$product['nama_produk'];?>">
        </div>
        <div class="flex-1">
            <?php
                    if(!empty(session()->getFlashdata('msg'))){ ?>

                
                    <div class="my-4 w-full py-2 px-6 border border-green-500 bg-green-400 text-white rounded-md">
                         <?php echo session()->getFlashdata('msg');?>
                    </div>

            <?php } ?>
           
            <div>
                <h1 class="capitalize text-4xl font-bold mb-4"><?=$product['nama_produk'];?></h1>
                <a href="/subcategory/product/<?=$permalink_subkategori?>" class="py-1 px-4 bg-gray-400 text-white rounded uppercase text-xs"><?=$sub_kategori?></a>
               <div class="mt-4 flex flex-col">
                    <h3 class="text-2xl mb-6">Rp. <?=number_format($product['harga_default_pcs'] , 0 , '.' , '.');?> /pcs</h3>
                    <div class="lg:ml-auto">
                        <form action="/produk/addcart" method="post">
                            <input type="number" name="qty" value="1" class="py-1 px-4 border border-red-700 rounded focus:outline-none text-sm mb-6">
                            <input type="hidden" name="idproduct" value="<?=$product['id'];?>">
                            <input type="hidden" name="permalink" value="<?=$product['permalink_product'];?>">
                            <button type="submit" class="py-1 px-4 text-sm bg-red-700 text-white rounded-md">Tambah Keranjang</button>
                        </form>
                    </div>
               </div>
                <hr class="mt-4" />
                <div class="mt-6">
                    <table class="w-full">
                        <tbody>
                            <tr class="border">
                                <td class="py-2 px-4">
                                    Material
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=ucwords($product['materials']);?></td>
                            </tr>
                            <tr class="border">
                                <td class="py-1 px-4">
                                    Kuantitas
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=$product['kuantitas'] == '0' ? '-' : $product['kuantitas'];?></td>
                            </tr>
                            <tr class="border">
                                <td class="py-1 px-4">
                                    Estimasi Selesai Produk
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=empty($product['estimasi_selesai_produksi']) ? '-' : $product['estimasi_selesai_produksi'].' Hari';?></td>
                            </tr>
                            <tr class="border">
                                <td class="py-1 px-4">
                                   Ukuran
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=empty($product['ukuran_produk']) ? '-' : $product['ukuran_produk'].' cm';?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php 
                    if(!empty($product['deskripsi_produk'])){
                ?>
                <div class="mt-4">
                    <h3 class="font-bold text-lg mb-4">Deskripsi Produk</h3>
                    <?=$product['deskripsi_produk'];?>
                </div>
                <?php }?>
            </div>
        </div>
   </div>
</section>
<!-- New Products -->


<?= $this->endSection()?>


<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
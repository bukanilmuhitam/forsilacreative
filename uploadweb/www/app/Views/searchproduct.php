<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>
<!-- This New Products -->
<section class="my-6 lg:my-16">
    <div class="flex space-x-6">
        <div class="flex-1">
            <p class="italic text-sm">Menampilkan <?=$countProduct?> hasil pencarian produk <span
                    class="font-bold">"<?=$textsearch?>"</span></p>
            <div class="mt-4">

                <!-- Show New Product -->
                <div class='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4'>
                   <?php echo view('card');?>
                </div>
                <!-- End Show -->

            </div>
        </div>
    </div>
</section>
<!-- New Products -->

<?= $this->endSection()?>


<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Order</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Order</p>
            </li>
            <li>
                <a href="/listorder">List Order</a>
            </li>
            <li>
                <p class="font-light text-gray-400"><?=$detail['number_invoice']?></p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Detail List Order <?=$detail['number_invoice'];?></h3>
        </div>
    </div>
    <div class="card-body">

        <h3 class="text-lg font-semibold">Informasi Order</h3>
        <table class=" mb-6">
            <tr>
                <td>Username</td>
                <td>:</td>
                <td><?=$detail['username'];?></td>
            </tr>
            <tr>
                <td>Alamat Pengiriman</td>
                <td>:</td>
                <td><?=$detail['alamat_pengiriman'];?></td>
            </tr>
            <tr>
                <td>Provinsi</td>
                <td>:</td>
                <td><?=$detail['provinsi'];?></td>
            </tr>
            <tr>
                <td>Kabupaten</td>
                <td>:</td>
                <td><?=$detail['kabupaten'];?></td>
            </tr>
            <tr>
                <td>Kecamatan</td>
                <td>:</td>
                <td><?=$detail['kecamatan'];?></td>
            </tr>
            <tr>
                <td>Kode Pos</td>
                <td>:</td>
                <td><?=$detail['kode_pos'];?></td>
            </tr>
        </table>

        <h3 class="text-lg font-semibold">Informasi Tambahan</h3>
        <table class=" mb-6">
            <tr>
                <td>Detail Pengiriman</td>
                <td>:</td>
                <td><?=!empty($detail['keterangan_tambahan_pengiriman']) ? $detail['keterangan_tambahan_pengiriman'] : '-';?></td>
            </tr>
            <tr>
                <td>Catatan Tambahan</td>
                <td>:</td>
                <td><?=!empty($detail['catatan_tambahan']) ? $detail['catatan_tambahan'] : '-';?></td>
            </tr>
        </table>

        <h3 class="text-lg font-semibold">Detail Produk</h3>
        <div class="mt-4 mb-6">
            <?php       
                        $total = 0;
                        $explode = explode(',' ,  $detail['product_id']);
                        $qty = explode(',' , $detail['qty_perproduct'] );
                        $nama_product = '';
                        foreach ($products as $product) {
                            for($i= 0 ; $i< count($explode); $i++){
                              if($product['id'] == $explode[$i]){

                                $subtotal = $qty[$i] * $product['harga_default_pcs'];
                                $total += $subtotal;
                                   
                       
            ?>
            <div class="w-full flex space-x-6 mb-6">
                <div class="w-32">
                    <div class="w-32 h-32 overflow-hidden">
                        <img src="<?=base_url()?>/upload/product/<?=$product['gambar_produk'];?>"
                            alt="<?=$product['nama_produk'];?>">
                    </div>
                </div>
                <div class="flex-1">
                    <h3 class="capitalize font-semibold text-2xl mb-2"><?=$product['nama_produk'];?></h3>
                    <p>Rp. <?=$product['harga_default_pcs'];?></p>
                    <div class="flex items-center space-x-4">
                            <span class="font-bold text-sm">Quantity : </span> 
                            <?=$qty[$i]?>
                    </div>
                    <p><span class="font-bold text-sm">Sub Total : Rp.
                                <?=number_format($subtotal , 0 , '.' , '.');?></span></p>
                </div>
            </div>

            <?php 
            
                        }
                    }
                }
            ?>
        </div>
        <h3 class="text-lg font-semibold">Total Pembayaran</h3>
        <table class="mb-6">
            <tr>
                <td>Total Belanja</td>
                <td>:</td>
                <td>Rp. <?=number_format($total , 0 , '.' , '.')?></td>
            </tr>
            <tr>
                <td>Biaya Shipping</td>
                <td>:</td>
                <td>Rp. <?=number_format($detail['tshipping'] , 0 , '.' , '.')?></td>
            </tr>
            <tr>
                <td>Total Pembayaran</td>
                <td>:</td>
                <td>Rp. <?php $tpyment = $total + $detail['tshipping']; echo number_format($tpyment , 0 , '.' , '.')?></td>
            </tr>
        </table>

    </div>
</div>
<?= $this->endSection() ?>
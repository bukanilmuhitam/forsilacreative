<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Order</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Order</p>
            </li>
            <li>
                <p class="font-light text-gray-400">Report Order</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card" id="app">
    <div class="card-header">
        <div class="title-header">
            <h3>{{title}} <span v-show="startdate != '' && enddate !='' ">{{ startdate }} s/d {{enddate}}</span></h3>
        </div>
    </div>
    <div class="card-body">

        <div class="flex space-x-4">
            <div class="flex items-center">
                <label for="" class="text-gray-400 mr-4">Start Date</label>
                <input type="date" name="startdate" v-model="startdate"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded">
            </div>
            <div class="flex items-center">
                <label for="" class="text-gray-400 mr-4">End Date</label>
                <input type="date" name="startdate" v-model="enddate"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded">
            </div>
            <div class="flex items-center" v-show="startdate != '' && enddate !=''">
                <button class="btn btn-primary" @click="showingdata(event)">View</button>
            </div>
        </div>

        <div class="mt-6" v-if="showdata">
            <!-- Table -->
            <table class="w-full">
                <thead>
                    <tr>
                        <th style="width:5%;" class="py-2 px-2 text-sm border">#</th>
                        <th class="py-2 px-2 text-sm border">Nomor Invoice</th>
                        <th style="width:20%;" class="py-2 px-2 text-sm border">Username</th>
                        <th style="width:20%;" class="py-2 px-2 text-sm border">Tanggal Order</th>
                        <th style="width:20%;" class="py-2 px-2 text-sm border">Payment</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(item , index) in items" :key="index">
                        <td class="py-2 px-2 text-sm border">{{ index + 1}}</td>
                        <td class="py-2 px-2 text-sm border">{{ item.number_invoice}}</td>
                        <td class="py-2 px-2 text-sm border text-center">{{ item.username}}</td>
                        <td class="py-2 px-2 text-sm border text-center">{{ item.date_order}}</td>
                        <td class="py-2 px-2 text-sm border text-center">Rp. {{ formatPrice(item.tpayment)}}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="py-2 px-2 text-sm border font-semibold">Total Belum Proses</td>
                        <td  class="py-2 px-2 text-sm border text-center font-semibold">Rp. {{ formatPrice(tBlmproses) }}</td>
                    </tr>
                    <!-- <tr>
                        <td colspan="4" class="py-2 px-2 text-sm border font-semibold">Total Barang Proses</td>
                        <td  class="py-2 px-2 text-sm border text-center font-semibold">Rp. {{ formatPrice(tProses) }}</td>
                    </tr>
                    <tr>
                        <td colspan="4" class="py-2 px-2 text-sm border font-semibold">Total Shipping</td>
                        <td  class="py-2 px-2 text-sm border text-center font-semibold">Rp. {{ formatPrice(tShipping) }}</td>
                    </tr> -->
                    <tr>
                        <td colspan="4" class="py-2 px-2 text-sm border font-semibold">Total Complete</td>
                        <td  class="py-2 px-2 text-sm border text-center font-semibold">Rp. {{ formatPrice(tComplete) }}</td>
                    </tr>
                    <tr class="bg-red-600 text-white">
                        <td colspan="4" class="py-2 px-2 text-sm border font-semibold">Total Semua Payment</td>
                        <td  class="py-2 px-2 text-sm border text-center font-semibold">Rp. {{ formatPrice(tBlmproses + tProses + tShipping + tComplete ) }}</td>
                    </tr>
                    <tr class="bg-green-600 text-white">
                        <td colspan="4" class="py-2 px-2 text-sm border font-semibold">Total Semua Payment</td>
                        <td  class="py-2 px-2 text-sm border text-center font-semibold">Rp. {{ formatPrice(tComplete) }}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="mt-6" v-else>
            <p>Tidak Ada data</p>
        </div>

    </div>
</div>

<script type="text/javascript">
window.general = <?php
          echo json_encode([
              'base_url' => base_url(),
              'uri' => $_SERVER["REQUEST_URI"],
          ]);
?>
</script>

<!-- VUE FILE -->
<script src="https://unpkg.com/vue@next"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
const App = {
    data() {
        return {
            title: 'Report Order',
            startdate: '',
            enddate: '',
            showdata: false,
            items: [],
            tBlmproses : 0,
            tProses : 0,
            tShipping : 0,
            tComplete : 0,
        }
    },
    methods: {
        formatPrice(value) {
			let val = (value / 1).toFixed(0).replace('.', ',')
			return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
		},
        getData(start, end) {

            const url = general.base_url + general.uri;
            // console.log(url);
            axios.post(`${url}/show`, {
                start: start,
                end: end,
            }).then(response => {

                console.log(response.data);
                const dt = response.data;
                if (dt.items === 0) {
                    this.showdata = false;
                } else {
                    this.showdata = true;
                    this.items = dt.items;
                    this.tBlmproses = dt.tBlmproses;
                    this.tProses = dt.tProses;
                    this.tShipping = dt.tShipping;
                    this.tComplete = dt.tComplete;
                }

            }).catch(function(error) {
                console.log(error);
            }).finally(() => {

            });

        },
        showingdata(event) {

            this.getData(this.startdate, this.enddate)

        }
    },
}

Vue.createApp(App).mount('#app')
</script>

<?= $this->endSection() ?>
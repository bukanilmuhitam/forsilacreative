<?= $this->extend('layout/layout_admin') ?>

<?php 
$uri = $_SERVER['REQUEST_URI'];
$getUri = explode('/', $uri);
?>

<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Product</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Product</p>
            </li>
            <li>
                <a href="/subcategory/show/<?=$getUri[3]?>" class="font-light">Sub Category</a>
            </li>
            <li>
                <p class="font-light text-gray-400">Tambah Data</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>



<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Tambah Data</h3>
        </div>
    </div>
    <div class="card-body">
        <form action="/subcategory/savedata" method="post">
            <div class="flex flex-col">
                <label for="">Category</label>
                <select name="category" class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded" >
                    <option value="">Pilih Kategori</option>
                    <?php 
                         foreach($categories as $category){
                    ?>
                        <option value="<?=$category['id']?>" <?=$category['id'] == $getUri[3] ? 'selected' : ''?>><?=$category['nama_category'];?></option>
                    <?php }?>
                </select>
            </div>
            <div class="my-6 flex flex-col">
                <label for="" class="text-gray-400">Nama Sub Category</label>
                <input type="text" name="name_subcategory"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('name_subcategory'))){ echo "invalid";}?>"
                    placeholder="Nama sub category" value="<?php echo set_value('name_subcategory'); ?>">
                <?php 
                            if(!empty($validation->getError('name_subcategory'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('name_subcategory')?></small>
                <?php }?>
            </div>
            <div class="my-4 flex space-x-2">
                    <a href="/category" class="btn btn-secondary">Cancel</a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
<?= $this->endSection() ?>
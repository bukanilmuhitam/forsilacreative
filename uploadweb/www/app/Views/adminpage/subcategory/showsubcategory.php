<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Product</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Product</p>
            </li>
            <li>
               <a href="/subcategory" class="font-light">Sub category</a>
            </li>
            <li>
                <p class="font-light text-gray-400">Detail sub kategori <?=$category['nama_category'];?></p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Sub Category product <?=$category['nama_category'];?></h3>
            <a href="/subcategory/add/<?=$id_category?>" class="py-1 px-4 bg-blue-600 text-xs ml-auto rounded text-white"><i
                    class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="card-body">

        <!-- Table -->
        <table class="w-full">
            <thead>
                <tr>
                    <th style="width:5%;" class="py-2 px-2 text-sm border">#</th>
                    <th class="py-2 px-2 text-sm border">Sub Kategori</th>
                    <th class="py-2 px-2 text-sm border">#</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if(!empty($items)){
                        $no= 1;
                        
                    

                        foreach($items as $item){
                            
                ?>
                <tr>
                    <td class="py-2 px-2 text-sm border text-center"><?=$no++?>. </td>
                    <td class="py-2 px-2 text-sm border">
                       <?=$item['nama_subcategory'];?>
                    </td>
                    <td class="py-2 px-2 text-sm border  text-center">
                        <button data-id="konfirmasidelete<?=$item['id'];?>" title="delete"
                            class="py-1 px-4 bg-red-600 text-white text-xs modal-btn rounded"><i
                                class="fa fa-trash"></i></button>&nbsp;
                        <a href="/subcategory/edit/<?=$item['id'];?>" title="Edit Data" class="py-1 px-4 bg-blue-600 text-white text-xs modal-btn rounded"><i
                                class="fa fa-edit"></i></a>    
                    </td>
                </tr>

                <div id="konfirmasidelete<?=$item['id'];?>" class="modal">
                    <div class="bg-white max-w-screen-sm mx-auto">
                        <div class="border border-gray-200">
                            <div class="p-4 flex justify-between">
                                <h3 class="font-semibold flex items-center">Konfirmasi</h3>
                                <span class="close font-bold text-2xl text-gray-300 flex items-center">&times;</span>
                            </div>
                        </div>
                        <form action="/subcategory/delete" method="post">
                            <div class="p-6">
                                Apakah anda yakin ingin menghapus data?
                                <input type="hidden" name="id" value="<?=$item['id'];?>">
                                <input type="hidden" name="id_category" value="<?=$id_category;?>">
                            </div>
                            <div class="border border-gray-200">
                                <div class="p-4">
                                    <button type="submit" class="btn btn-success">Ya, saya ingin menghapus data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                
                <?php }}else{?>
                <tr>
                    <td colspan="4" class="py-2 px-2 text-sm border  font-light text-center">Tidak ada data ditemukan
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <!-- End Table -->

    </div>
</div>
<?= $this->endSection() ?>
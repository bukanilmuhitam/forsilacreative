<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Product</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Product</p>
            </li>
            <li>
                <p class="font-light text-gray-400">Category</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Category product</h3>
            <a href="/category/add" class="py-1 px-4 bg-blue-600 text-xs ml-auto rounded text-white"><i
                    class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="card-body">

        <!-- Table -->
        <table class="w-full">
            <thead>
                <tr>
                    <th style="width:5%;" class="py-2 px-2 text-sm border">#</th>
                    <th class="py-2 px-2 text-sm border">Nama categori</th>
                    <th style="width:20%;"  class="py-2 px-2 text-sm border">#</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if(!empty($categories)){
                        $no= 1;
                        foreach($categories as $category){
                ?>
                <tr>
                    <td class="py-2 px-2 text-sm border text-center"><?=$no++?>. </td>
                    <td class="py-2 px-2 text-sm border"><?=$category['nama_category'];?></td>
                    <td class="py-2 px-2 text-sm border flex justify-center">
                        <button data-id="konfirmasidelete<?=$category['id'];?>" title="delete"
                            class="py-1 px-4 bg-red-600 text-white text-xs modal-btn rounded"><i
                                class="fa fa-trash"></i></button>&nbsp;
                        <a href="/category/edit/<?=$category['id'];?>" title="Edit Data" class="py-1 px-4 bg-blue-600 text-white text-xs modal-btn rounded"><i
                                class="fa fa-edit"></i></a>
                    </td>
                </tr>
                <div id="konfirmasidelete<?=$category['id'];?>" class="modal">
                    <div class="bg-white max-w-screen-sm mx-auto">
                        <div class="border border-gray-200">
                            <div class="p-4 flex justify-between">
                                <h3 class="font-semibold flex items-center">Konfirmasi</h3>
                                <span class="close font-bold text-2xl text-gray-300 flex items-center">&times;</span>
                            </div>
                        </div>
                        <form action="/category/delete" method="post">
                            <div class="p-6">
                                Apakah anda yakin ingin menghapus data?
                                <input type="hidden" name="id" value="<?=$category['id'];?>">
                            </div>
                            <div class="border border-gray-200">
                                <div class="p-4">
                                    <button type="submit" class="btn btn-success">Ya, saya ingin menghapus data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php }}else{?>
                <tr>
                    <td colspan="3" class="py-2 px-2 text-sm border  font-light text-center">Tidak ada data ditemukan
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <!-- End Table -->

    </div>
</div>
<?= $this->endSection() ?>
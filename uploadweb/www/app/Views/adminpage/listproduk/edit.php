<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0 z-40">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Product</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Product</p>
            </li>
            <li>
                <a href="/listproduk" class="font-light">List Product</a>
            </li>
            <li>
                <p class="font-light text-gray-400">Edit Data</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>
<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Edit Data</h3>
        </div>
    </div>
    <div class="card-body">
        <form action="/listproduk/updatedata" method="post" enctype="multipart/form-data">
            <div class="w-full">
                <div class="flex space-x-4">
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Nama Produk</label>
                        <input type="text" name="nama_produk"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('nama_produk'))){ echo "invalid";}?>"
                            placeholder="Nama produk" value="<?php echo $detail['nama_produk'] ?>">
                        <?php 
                            if(!empty($validation->getError('nama_produk'))) {
                        ?>
                        <small style="color:red;"><?=$validation->getError('nama_produk')?></small>
                        <?php }?>
                    </div>
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Materials produk</label>
                        <input type="text" name="materials"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded"
                            placeholder="Materials produk" value="<?php echo $detail['materials'] ?>">
                    </div>
                </div>
            </div>
            <input type="hidden" name="id" value="<?=$detail['id']?>">
            <div class="my-6 w-full">
                <div class="flex space-x-4">
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Kategori</label>
                        <select name="kategori" id="kategori"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded">
                            <option value="">Pilih Kategori</option>
                        </select>
                    </div>
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Sub Kategori</label>
                        <select name="subkategori" id="subkategori"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded"
                            disabled>
                            <option value="">Pilih Subkategori</option>
                        </select>
                    </div>
                </div>
                <small>NB. Pilih kategori dan sub kategori jika ingin ubah kategori dan sub category. kosongkan jika tidak ingin ubah kategori dan sub category</small>
            </div>

            <div class="my-6 w-full">
                <div class="flex space-x-4">
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Kuantitas produk</label>
                        <input type="text" name="kuantitas_produk"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded"
                            placeholder="Kuantitas produk" value="<?php echo $detail['kuantitas'] ?>">
                    </div>
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Estimasi Selesai Produksi</label>
                        <div class="flex items-center space-x-2">
                            <input type="number" name="estimasi_selesai_produksi"
                                class="flex flex-1 py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded"
                                placeholder="Estimasi Selesai Produksi" value="<?php echo $detail['estimasi_selesai_produksi'] ?>">
                            <span>Hari</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-6 w-full">
                <div class="flex space-x-4">
                    <!-- <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Type ukuran</label>
                        <select name="type_ukuran"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded">
                            <option value="normal">Normal</option>
                            <option value="sedang">Sedang</option>
                            <option value="besar">Besar</option>
                        </select>
                    </div> -->
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Ukuran</label>
                        <div class="flex items-center space-x-2">
                            <input type="text" name="ukuran"
                                class="flex py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded"
                                placeholder="Panjang produk" value="<?php echo $detail['ukuran_produk'] ?>">
                            <span>cm</span>
                        </div>
                    </div>
                    <div class="flex flex-col w-1/2">
                        <label for="" class="text-gray-400">Berat</label>
                        <div class="flex items-center space-x-2">
                            <input type="text" name="berat_produk"
                                class="flex py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded"
                                placeholder="Panjang produk" value="<?php echo $detail['berat_product'] ?>">
                            <span>gram</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="flex flex-col my-6">
                <label for="" class="text-gray-400">Harga Produk</label>
                <input type="text" name="harga_produk"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('harga_produk'))){ echo "invalid";}?>"
                    placeholder="Harga produk" value="<?php echo $detail['harga_default_pcs'] ?>">
                <?php 
                            if(!empty($validation->getError('harga_produk'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('harga_produk')?></small>
                <?php }?>
            </div>
            <div class="flex flex-col my-6">
                <label for="" class="text-gray-400">Deskripsi produk</label>
                <textarea name="deskripsi_produk">
                    <?=$detail['deskripsi_produk'];?>
                </textarea>
            </div>

            <div class="flex flex-col my-6">
                <label for="" class="text-gray-400">Gambar produk</label>
                <input type="file" name="gambar_produk"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('gambar_produk'))){ echo "invalid";}?>"
                     value="<?php echo set_value('gambar_produk'); ?>">
                <?php 
                            if(!empty($validation->getError('gambar_produk'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('gambar_produk')?></small>
                <?php }?>
                <small>*Kosongkan jika tidak ingin merubah gambar</small>
            </div>


            <div class="my-4 flex space-x-2">
                <a href="/listproduk" class="btn btn-secondary">Cancel</a>
                <button type="submit" class="btn btn-primary">Update</button>
            </div>
        </form>
    </div>
</div>

<script src="https://cdn.ckeditor.com/4.15.1/standard/ckeditor.js"></script>
<script>
var request = new XMLHttpRequest();

request.open('GET', 'http://localhost:8080/api/getCategory', true);
request.onload = function() {
    // Begin accessing JSON data here
    var data = JSON.parse(this.response);
    const kategori = document.querySelector('#kategori');
    //console.log(kategori);

    if (request.status >= 200 && request.status < 400) {

        data.forEach((item) => {

            kategori.innerHTML += `
            <option value='${item.id}'>${item.nama_category}</option>
        `;

        });

    } else {

        console.log('error');

    }

}

request.send();

// Get Sub category
document.getElementById('kategori').addEventListener('change', function() {
    // console.log('You selected: ', this.value);
    // Get sub category
    document.getElementById('subkategori').disabled = false;
    request.open('GET', `http://localhost:8080/api/getSubcategory?id_category=${this.value}`, true);
    request.onload = function() {
        // Begin accessing JSON data here
        var data = JSON.parse(this.response);
        const subkategori = document.querySelector('#subkategori');
        // console.log(data);
        subkategori.innerHTML = `<option value="">Pilih Subkategori</option>`;
        if (request.status >= 200 && request.status < 400) {

            data.forEach((item) => {

                subkategori.innerHTML += `
                    <option value='${item.id}'>${item.nama_subcategory}</option>
                `;

            });

        } else {

            console.log('error');

        }

    }
    request.send();

});

CKEDITOR.replace('deskripsi_produk');
</script>

<?= $this->endSection() ?>
<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Produk</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Produk</p>
            </li>
            <li>
                <a href="/listproduk">List Produk</a>
            </li>
            <li>
                <p class="font-light text-gray-400"><?=$detail['nama_produk'];?></p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3 class="capitalize">Detail List Produk <?=$detail['nama_produk'];?></h3>
        </div>
    </div>
    <div class="card-body">

    <div class="flex space-x-8">
        <div class="w-2/5 overflow-hidden">
            <img src="<?=base_url()?>/upload/product/<?=$detail['gambar_produk'];?>" alt="<?=$detail['nama_produk'];?>">
        </div>
        <div class="flex-1">
        <div>
                <h1 class="capitalize text-4xl font-bold mb-4"><?=$detail['nama_produk'];?></h1>
                <span class="py-1 px-4 bg-gray-400 text-white rounded uppercase text-xs"><?=$sub_kategori?></span>
               <div class="mt-4 flex">
                    <h3 class="text-2xl">Rp. <?=number_format($detail['harga_default_pcs'] , 0 , '.' , '.');?> /pcs</h3>
               </div>
                <hr class="mt-4" />
                <div class="mt-6">
                    <table class="w-full">
                        <tbody>
                            <tr class="border">
                                <td class="py-2 px-4">
                                    Material
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=ucwords($detail['materials']);?></td>
                            </tr>
                            <tr class="border">
                                <td class="py-1 px-4">
                                    Kuantitas
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=$detail['kuantitas'] == '0' ? '-' : $detail['kuantitas'];?></td>
                            </tr>
                            <tr class="border">
                                <td class="py-1 px-4">
                                    Estimasi Selesai Produk
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=empty($detail['estimasi_selesai_produksi']) ? '-' : $detail['estimasi_selesai_produksi'].' Hari';?></td>
                            </tr>
                            <tr class="border">
                                <td class="py-1 px-4">
                                   Ukuran
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=empty($detail['ukuran_produk']) ? '-' : $detail['ukuran_produk'].' cm';?></td>
                            </tr>
                            <tr class="border">
                                <td class="py-1 px-4">
                                   Berat Produk
                                </td>
                                <td class="py-1 px-4">:</td>
                                <td class="py-1 px-4 text-md"><?=empty($detail['berat_product']) ? '-' : $detail['berat_product'].' gram';?></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <?php 
                    if(!empty($detail['deskripsi_produk'])){
                ?>
                <div class="mt-4">
                    <h3 class="font-bold text-lg mb-4">Deskripsi Produk</h3>
                    <?=$detail['deskripsi_produk'];?>
                </div>
                <?php }?>
            </div>
        </div>
    </div>     

    </div>
</div>
<?= $this->endSection() ?>
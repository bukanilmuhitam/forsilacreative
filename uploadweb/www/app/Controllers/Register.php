<?php namespace App\Controllers;

class Register extends BaseController
{

    public function index(){


        if(!empty(session()->get('token'))){
            // maka redirct ke halaman login
            return redirect()->to('/validasi');
        }

        if(! $this->validate([]))
        {
            $data= [
				'title' => 'Register Member Forsila Creative',
                'validation' => $this->validator,
            ];
            return view('user/auth/register' , $data);
        }
    }

	public function regist()
	{

        if(!empty(session()->get('token'))){
            // maka redirct ke halaman login
            return redirect()->to('/validasi');
        }

        $validated = $this->validate([
                'fullname' => 'required|max_length[30]',
                'no_hp' => 'max_length[13]|numeric',
                'email' => 'required|is_unique[user_member.email]|valid_email',
                'password' => 'required|min_length[8]|matches[repeat_password]',
            ],
            [
                'fullname' => [
                    'required' => 'Username wajib diisi',
                    'max_length' => 'Tidak lebih dari 30 karakter',
                ],
                'no_hp' => [
                    'max_length' => 'Tidak lebih dari 13 karakter',
                    'numeric' => 'Hanya boleh diisi angka',
                ],
                'email' => [
                    'required' => 'Username wajib diisi',
                    'is_unique' => 'Email anda telah digunakan',
                    'valid_email' => 'Email Anda Tidak Valid',
                ],
                'password' => [
                    'required' => 'Password wajib diisi',
                    'min_length' => 'Password Harus Lebih Dari 8 Karakter',
                ],
            ]
        );
        if ($validated == FALSE) {
            
            return $this->index();

        }else{
            $session = session();
            $email = $this->request->getPost('email');
            $password = md5($this->request->getPost('password'));
            $fullname = $this->request->getPost('fullname');
            $no_hp = $this->request->getPost('no_hp');
            $username = $this->createusername($fullname);
            $token = $this->createtoken($fullname);
            $data = [
                'email' => $email,
                'username' => $username,
                'password' => $password,
                'no_handphone' => $no_hp,
                'register_date' => date('Y-m-d H:i:s'),
                'status_user' => 0,
                'nama_user' => $fullname,
            ];

            $pesan = "Hallo $fullname , Berikut Token Validasimu : $token"; 

           

            $res = $this->member->saveData($data);
            if($res == 'ok'){
                
                $this->sendemail('Token Validasi Registrasi' , $email , $pesan);
                $ses_data = [
                    'email'     => $email,
                    'token'     => $token,
                ];
                $session->set($ses_data);
                 session()->setFlashdata('success', 'Berhasil Mendaftar. Segera Cek Email untuk melihat token');
                 return redirect()->to('/validasi');
             }else{
                 session()->setFlashdata('error', 'Gagal Menambahkan data!');
                 return redirect()->to('/validasi');
             }
            
          
        }
    }
    
   


}

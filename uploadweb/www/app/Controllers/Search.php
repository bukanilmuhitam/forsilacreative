<?php namespace App\Controllers;

class Search extends BaseController
{
    
	public function index()
	{
		echo $this->error_not_found();
    }
    
    public function product(){

        $textsearch = $this->request->getPost('textsearch');
        $products = $this->product->searchdata($textsearch);

        $categories = $this->CategoryModel->getData();
        $subcategory = $this->subkategori->getData();

          // Cart
          $username = session()->get('user_name');
          $cart = $this->cart->check_cart($username);
          if(empty($cart)){
              $countcart = 0;
          }else{
              $countcart = count($cart);
          }

        if(empty($products)){
            echo $this->error_not_found("Mohon maaf produk $textsearch tidak dapat ditemukan :(");
        }else{
            $data = [
                'title'=>"Cari data produk $textsearch",
                'menus' => $categories,
                'submenus' => $subcategory,
                'products' => $products,
                'textsearch' => $textsearch,
                'countProduct' => count($products), 
                'cart' => $cart,
			    'countcart' => $countcart,
            ];
            // dd($similiar_category);
            return view('searchproduct' , $data);
        }

    }


}
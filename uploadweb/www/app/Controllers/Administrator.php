<?php namespace App\Controllers;

class Administrator extends BaseController
{
	public function index()
	{
		   if(session()->get('unlock') == FALSE){
				// maka redirct ke halaman login
				return redirect()->to('/unlockapk'); 
			}

			if(session()->get('logged_in') == FALSE){
				return redirect()->to('/auth');
			}
			
			return view('adminpage/home/index');
	}

	//--------------------------------------------------------------------

}

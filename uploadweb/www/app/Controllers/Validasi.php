<?php namespace App\Controllers;

class Validasi extends BaseController
{
	public function index()
	{
        if(empty(session()->get('token'))){
            // maka redirct ke halaman login
            return redirect()->to('/register');
        }
		return view('user/auth/validasi');
    }
    
    public function proses(){
        $session = session();
        $token = $this->request->getPost('token');
        $tokensession = $session->get('token');
        $email = $session->get('email');
        if($token == $tokensession){

            $Detail = $this->member->where('email' , $email)->first();
            $idUser = $Detail['id'];
            $nama = $Detail['nama_user'];
            $data = [
                'status_user' => 1,
            ];
           
            $res = $this->member->updatedata($idUser , $data);
            if($res == 'ok'){

                $pesan = "
                Hallo $nama, Account kamu telah aktif.
                <br /> nikmatin desain dan ide menarik untuk pesonal branding produk anda, Dan temui hal menarik lainnya.
                <br /><br />
                Terimakasih.
                <br /><br />
                Salam hangat,
                <br/><br />
                Forsila Team
                ";
                $this->sendemail('Pendaftaran Selesai' , $email , $pesan);
              
                $ses_data = ['email', 'token'];
                $session->remove($ses_data);

                $create_seasonlog = [
                    'login_user' => TRUE,
                    'user_name' => $Detail['username'],
                ];
                $session->set($create_seasonlog);
                return redirect()->to('/');
            }else{
                return redirect()->to('/');
            }



        }else{

            session()->setFlashdata('error', 'Token salah!');
            return redirect()->to('/validasi');

        }

    }


}

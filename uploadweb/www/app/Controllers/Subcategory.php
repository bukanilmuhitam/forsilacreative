<?php namespace App\Controllers;

class Subcategory extends BaseController
{
	public function index(){
        
        
        // if(!empty($uri = $this->request->uri->getSegments())){
        //     $getID= $uri[1];
        // }else{

            if(session()->get('unlock') == FALSE){
                // maka redirct ke halaman login
                return redirect()->to('/unlockapk'); 
            }

            if(session()->get('logged_in') == FALSE){
                return redirect()->to('/auth');
            }


            $data= [
                'title' => 'Subcategori Produk | Forsila Creative',
                'items' => $this->subkategori->getData2(),
                'counts' => $this->subkategori->getData(),
                'categories' =>$this->CategoryModel->getData(),
            ];
            return view('adminpage/subcategory/index' , $data);

        // }
    
    }

    public function show(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $uri = $this->request->uri->getSegments();
        $getID= $uri[2];
        
        $data= [
            'title' => 'Lihat sub kategori | Forsila Creative',
            'items' =>  $this->subkategori->getByIdCategory($getID),
            'category' => $this->CategoryModel->getData($getID),
            'id_category' => $getID,
        ];
		return view('adminpage/subcategory/showsubcategory' , $data);

    }

    public function product(){
        $uri = $this->request->uri->getSegments();
        $getID= $uri[2];
        $Detailsubcategory = $this->subkategori->getByPermalink($getID); 
        $idSubcategory = $Detailsubcategory['id'];
        $namasub = $Detailsubcategory['nama_subcategory'];
        $products = $this->product->getProductBySubcategory($idSubcategory);

        // Similiar Kategori
        $idcategory = $Detailsubcategory['id_category'];
        $similiar_category = $this->subkategori->getByIdCategory($idcategory);

        if(empty($products)){
            echo $this->error_not_found("Mohon maaf produk pada sub category $namasub belum tersedia :(");
        }else{

            $session = session();
            $categories = $this->CategoryModel->getData();
            $subcategory = $this->subkategori->getData();
            
            // Cart
            $username = session()->get('user_name');
            $cart = $this->cart->check_cart($username);
            if(empty($cart)){
				$countcart = 0;
			}else{
				$countcart = count($cart);
			}
       
            $data = [
                'title'=>'Percetakan Forsila Creative',
                'menus' => $categories,
                'submenus' => $subcategory,
                'products' => $products,
                'view_by' => 'sub category',
                'nama_subcategory' => $Detailsubcategory['nama_subcategory'],
                'countProduct' => count($products), 
                'similiar_categories' => $similiar_category,
                'cart' => $cart,
			    'countcart' => $countcart,
            ];
            // dd($similiar_category);
            return view('user/productview/bysubcategory' , $data);
        }
        
    }
    
    public function add(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        if(! $this->validate([])){ 
            $data= [
                'title' => 'Tambah Subcategory Produk | Forsila Creative',
                'categories' => $this->CategoryModel->getData(),
                'validation' => $this->validator,
            ];
            return view('adminpage/subcategory/add' , $data);
        }
    }

    public function savedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
            'name_subcategory' => 'required|max_length[30]',
        ],
        [
            'name_subcategory' => [
                'required' => 'Nama Subcategory harus diisi',
                'max_length' => 'Nama Subcategory tidak boleh lebih dari 30'
            ],
        ]
        );
        if ($validated == FALSE) {
            
            return $this->add();

        }else{
            
            $name_subcategory = $this->request->getPost('name_subcategory');
            $permalink = str_replace(' ' , '-' , $name_subcategory);
            $idcategory = $this->request->getPost('category');
           
            $data = [ 
                'id_category' => $this->request->getPost('category'),
                'nama_subcategory' => $name_subcategory,
                'permalink' => $permalink,
            ];
            
           $res = $this->subkategori->saveData($data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Menambahkan data!');
                return redirect()->to("/subcategory/show/$idcategory");
            }else{
                session()->setFlashdata('error', 'Gagal Menambahkan data!');
                return redirect()->to("/subcategory/show/$idcategory");
            }

        }
    }

    public function edit(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }
        
        if(! $this->validate([]))
        {
           
            $uri = $this->request->uri->getSegments();
            $getID= $uri[2];
            $getmodel = $this->subkategori->getData($getID);
            $idcategory = $getmodel['id_category'];
    
            $data= [
                'title' => 'Edit Subcategory Produk | Forsila Creative',
                'categories' => $this->CategoryModel->getData(),
                'detail' => $getmodel,
                'id_category' => $idcategory,
                'validation' => $this->validator,
            ];
            return view('adminpage/subcategory/edit' , $data);
        }

    }

    public function updatedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

       
        $validated = $this->validate([
            'name_subcategory' => 'required|max_length[30]',
        ],
        [
            'name_subcategory' => [
                'required' => 'Nama Subcategory harus diisi',
                'max_length' => 'Nama Subcategory tidak boleh lebih dari 30'
            ],
        ]
        );
        if ($validated == FALSE) {
            
            return $this->edit();

        }else{
            
            $id = $this->request->getPost('id');
            $name_subcategory = $this->request->getPost('name_subcategory');
            $permalink = str_replace(' ' , '-' , $name_subcategory);
            $idcategory = $this->request->getPost('category');
            
           
            $data = [ 
                'id_category' => $this->request->getPost('category'),
                'nama_subcategory' => $name_subcategory,
                'permalink' => $permalink,
            ];

        //    dd($id);
           $res = $this->subkategori->updatedata( $id ,$data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Update data!');
                return redirect()->to("/subcategory/show/$idcategory");
            }else{
                session()->setFlashdata('error', 'Gagal Update data!');
                return redirect()->to("/subcategory/show/$idcategory");
            }

        }


    }

    public function delete(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $id = $this->request->getPost('id');
        $id_category = $this->request->getPost('id_category');
        $res = $this->subkategori->deletedata($id);

        if($res == 'ok'){
            session()->setFlashdata('success', 'Berhasil hapus data!');
            return redirect()->to("/subcategory/show/$id_category");
        }else{
            session()->setFlashdata('error', 'Gagal hapus data!');
            return redirect()->to("/subcategory/show/$id_category");
        }

    }


}

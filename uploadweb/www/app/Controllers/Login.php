<?php namespace App\Controllers;

class Login extends BaseController
{
	public function index()
	{
		
        if(! $this->validate([]))
        {
            $data= [
				'title' => 'Login User forsila creative',
                'validation' => $this->validator,
            ];
            return view('user/auth/login' , $data);
        }
    }
    
    public function proses(){

        $validated = $this->validate([
            'uname' => 'required|valid_email',
            'password' => 'required',
        ],
        [
            'uname' => [
                'required' => 'Email wajib diisi',
                'valid_email' => 'Email harus valid',
            ],
            'password' => [
                'required' => 'Password wajib diisi',
            ],
        ]
        );
        if ($validated == FALSE) {
            
            return $this->index();

        }else{
            $session = session();
            $uname = $this->request->getPost('uname');
            $password = md5($this->request->getPost('password'));
            // die();
            $getmodel =  $this->member->where('email' , $uname)->first();
            
            // var_dump($getmodel);
            // die();
            if($getmodel){
                $pass = $getmodel['password'];
                if($password == $pass){
                    $ses_data = [
                        'user_name'     => $getmodel['username'],
                        'login_user'     => TRUE,
                    ];
                    $session->set($ses_data);
                    return redirect()->to('/');
                }else{
                    $session->setFlashdata('msg', 'Password tidak sesuai');
                    return redirect()->to('/login');
                }

            }else{
                $session->setFlashdata('msg', 'Akun tidak ditemukan');
                return redirect()->to('/login');
            }
        }

    }


}
<?php namespace App\Controllers;

class Profile extends BaseController
{
	public function index(){   
        $session = session();
		// This Get Menu Dynamic
		// Get menu by category
		$categories = $this->CategoryModel->getData();
		$subcategory = $this->subkategori->getData();

		// Cart
		$username = session()->get('user_name');
		$cart = $this->cart->check_cart($username);

		if(empty($cart)){
			$countcart = 0;
		}else{
			$countcart = count($cart);
		}

		
		// Orderan belum diproses
		$blmproses = $this->order->getOrderBelumProses($username);
		$proses = $this->order->getOrderProses($username);
		$shipping = $this->order->getOrderShipping($username);
		$complete = $this->order->getOrderComplete($username);
		$batal = $this->order->getOrderBatal($username);

		$data = [
			'title'=>'Profile',
			'menus' => $categories,
			'submenus' => $subcategory,
			'cart' => $cart,
			'countcart' => $countcart,
			'belumproses' => $blmproses,
			'jumlahbelumproses' => count($blmproses),
			'proses' => $proses,
			'jumlahproses' => count($proses),
			'shipping' => $shipping,
			'jumlahshipping' => count($shipping),
			'complete' => $complete,
			'jumlahcomplete' => count($complete),
			'batal' => $batal,
			'jumlahbatal' => count($batal),
		];
		
        return view('user/profile/index' , $data);
	}

	public function detailtransaksi(){
		$uri = $this->request->uri->getSegments();
		$getID= $uri[2];
		$session = session();
		// This Get Menu Dynamic
		// Get menu by category
		$categories = $this->CategoryModel->getData();
		$subcategory = $this->subkategori->getData();

		// Cart
		$username = session()->get('user_name');
		$cart = $this->cart->check_cart($username);

		if(empty($cart)){
			$countcart = 0;
		}else{
			$countcart = count($cart);
		}

		$Detail = $this->order->getDataByInvoiceNumber($getID);

		$data = [
			'title'=>'Profile',
			'menus' => $categories,
			'submenus' => $subcategory,
			'cart' => $cart,
			'countcart' => $countcart,
			'number_invoice' => $getID,
			'detail' => $Detail,
			'products' => $this->product->getData(),
		];
		
        return view('detailorder' , $data);
	}

	public function gantipassword(){

		$session = session();
		// This Get Menu Dynamic
		// Get menu by category
		$categories = $this->CategoryModel->getData();
		$subcategory = $this->subkategori->getData();

		// Cart
		$username = session()->get('user_name');
		$cart = $this->cart->check_cart($username);

		if(empty($cart)){
			$countcart = 0;
		}else{
			$countcart = count($cart);
		}
		if(! $this->validate([])){ 

		$data = [
			'title'=>'Profile Ganti Password',
			'menus' => $categories,
			'submenus' => $subcategory,
			'cart' => $cart,
			'countcart' => $countcart,
			'validation' => $this->validator,
		];
		
			return view('user/profile/gantipassword.php' , $data);
		}
	}

	public function prosesgantipassword(){

		$validated = $this->validate([
			'password_baru' => 'required|min_length[8]|matches[repeat_password]',
			],
			[
				'password_baru' => [
					'required' => 'Password wajib diisi',
					'min_length' => 'Password Harus Lebih Dari 8 Karakter',
					'matches' => 'Password kamu tidak sama',
				],
			]
		);
		if ($validated == FALSE) {
			
			return $this->gantipassword();

		}else{

			$password_lama = md5($this->request->getPost('password_lama'));
			$password_baru = md5($this->request->getPost('password_baru'));
			$session = session();
	
			// Cart
			$username = session()->get('user_name');
			$Detail = $this->member->getDataByUsername($username);
			$passworddb = $Detail['password'];
			$email = $Detail['email'];
			if($password_lama == $passworddb){

				$data = [
                    'password' => $password_baru,
                ];

            

            //    dd($password);

			$res = $this->member->where('email' , $email)->set($data)->update();
			if($res == 'ok'){
				session()->setFlashdata('sukses', 'Berhasil ganti password');
                return redirect()->to('/profile/gantipassword');
			}

			}else{
				session()->setFlashdata('error', 'Password lama tidak sesuai');
                return redirect()->to('/profile/gantipassword');
			}

		}

	}


}

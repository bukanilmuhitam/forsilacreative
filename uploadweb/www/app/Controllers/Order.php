<?php namespace App\Controllers;

class Order extends BaseController
{
	public function index()
	{
		$session = session();
		// This Get Menu Dynamic
		// Get menu by category
		$categories = $this->CategoryModel->getData();
		$subcategory = $this->subkategori->getData();

		// Cart
		$username = session()->get('user_name');
		$cart = $this->cart->check_cart($username);
        $provinsi = $this->getProvinsi();
      
		if(empty($cart)){
           
            echo $this->error_not_found("Orderan anda kosong :(");

		}else{
            $countcart = count($cart);
            
            // Get New Products
            $produk = $this->product->getData();
            
            if(! $this->validate([])){ 
                $data = [
                    'title'=>'Order',
                    'menus' => $categories,
                    'submenus' => $subcategory,
                    'products' => $produk,
                    'cart' => $cart,
                    'countcart' => $countcart,
                    'provinsi' => $provinsi->rajaongkir->results,
                    'validation' => $this->validator,
                ];
                
                return view('order' , $data);
            }
		}

    }
    
    
    public function saveorder(){

        $validated = $this->validate([
                'alamat_pengiriman' => 'required',
                'provinsi' => 'required',
                'kabupaten' => 'required',
                'kodepos' => 'required',
            ],
            [
                'alamat_pengiriman' => [
                    'required' => 'Alamat pengiriman wajib diisi',
                ],
                'provinsi' => [
                    'required' => 'Provinsi pengiriman wajib diisi',
                ],
                'kabupaten' => [
                    'required' => 'Kabupaten pengiriman wajib diisi',
                ],
                'kodepos' => [
                    'required' => 'Kodepos pengiriman wajib diisi',
                ],
            ]
        );

        if ($validated == FALSE) {
            
            return $this->index();

        }else{

            $session = session();
            $number_invoice = "INVOICE-".rand( 100 , 2000);
            $idproduct = $this->converttexttoarray($this->request->getPost('id_product'));
            $username = $session->get('user_name');
            $alamat_pengiriman = $this->request->getPost('alamat_pengiriman');
            $getprovinsi = explode('-', $this->request->getPost('provinsi'));
            $provinsi = $getprovinsi[1];
            $getkabupaten = explode( '-', $this->request->getPost('kabupaten'));
            $kabupaten = $getkabupaten[1];
            // $kecamatan = $this->request->getPost('kecamatan');
            $kodepos = $this->request->getPost('kodepos');
            $detail_alamat = $this->request->getPost('detail_alamat');
            $catatan_tambahan = $this->request->getPost('catatan_tambahan');
            $qtyperproduct = $this->converttexttoarray($this->request->getPost('qtyperproduct'));
            $tshipping = $this->request->getPost('ongkir');
            
            
            $total = 0;
            for ($i=0; $i < count($idproduct); $i++) { 
                $product = $this->product->getData($idproduct[$i]);
                $subtotal = $qtyperproduct[$i] * $product['harga_default_pcs'];
                $total += $subtotal;
            }
            
            $tpayment = $tshipping + $total;

            $gambar_design = $this->request->getFile('gambar_design');
            
            // var_dump($gambar_design->getName());
            // die();
            if($gambar_design->getName() != ''){

                $nama_gambar_design = $gambar_design->getName();
                $new_image = $this->changenameimage($nama_gambar_design);
                $gambar_design->move(ROOTPATH .'public/upload/lampiran_design/' , $new_image);
    

                $data= [
                    'number_invoice' => $number_invoice,
                    'product_id' => $this->request->getPost('id_product'),
                    'username' => $username,
                    'alamat_pengiriman' => $alamat_pengiriman,
                    'provinsi' => $provinsi,
                    'kabupaten' => $kabupaten,
                    // 'kecamatan' => $kecamatan,
                    'kode_pos' => $kodepos,
                    'keterangan_tambahan_pengiriman' => $detail_alamat,
                    'tshipping' => $tshipping,
                    'tpayment' => $tpayment,
                    'catatan_tambahan' => $catatan_tambahan,
                    'status_order' => 0,
                    'date_order' => date('Y-m-d'),
                    'qty_perproduct' => $this->request->getPost('qtyperproduct'),
                    'lampiran_file' => $new_image,
                ];
            }else{

                $data= [
                    'number_invoice' => $number_invoice,
                    'product_id' => $this->request->getPost('id_product'),
                    'username' => $username,
                    'alamat_pengiriman' => $alamat_pengiriman,
                    'provinsi' => $provinsi,
                    'kabupaten' => $kabupaten,
                    // 'kecamatan' => $kecamatan,
                    'kode_pos' => $kodepos,
                    'keterangan_tambahan_pengiriman' => $detail_alamat,
                    'tshipping' => $tshipping,
                    'tpayment' => $tpayment,
                    'catatan_tambahan' => $catatan_tambahan,
                    'status_order' => 0,
                    'date_order' => date('Y-m-d'),
                    'qty_perproduct' => $this->request->getPost('qtyperproduct'),
                ];

            }
            
            // dd($data);

            // GET USER
            $user = $this->member->getDataByUsername($username);
            $email = $user['email'];
            $namauser = $user['nama_user'];

            $totalpy = number_format($tpayment , 0 , '.' , '.');
            $pesan = "
            Hallo $namauser, Orderderan kamu dengan nomor invoice : $number_invoice telah kami terima.
            <br />
            <br />
            Kamu bisa melakukan pembayaran lewat transfer bank, melalui rekening dibawah ini :
            <br/>
            <br /> 
            BANK TESTING 1809-0909-2987
            <br />
            <br />
            Dengan Total Payment : Rp. $totalpy
            <br />
            <br />
            Lampirkan bukti pembayaran dengan membalas email ini agar barang kamu dapat kami proses.
            <br />
            <br />
            Jika ingin menambahkan lampiran file desain tambahan hubungi customer service kami melalui whastapp 0865-9876-0909, dengan format #LAMPIRAN FILE DESIGN NOMOR INVOICE : $number_invoice
            <br /><br />
            Terimakasih.
            <br /><br />
            Salam hangat,
            <br/><br />
            Forsila Team
            ";

            $res = $this->order->saveData($data);
			if($res == 'ok'){
                $this->sendemail('Order Sukses' , $email , $pesan);
                $this->cart->deleteByUsername($username);
				session()->setFlashdata('success', 'Sukses');
                session()->setFlashdata('totalBayar' , $tpayment);
				return redirect()->to("/order/orderfinish");
			}

            
            // dd($email);
           
            

        }

    }

    public function orderfinish(){

        if(!empty(session()->getFlashdata('success'))){
            return view('finishorder');
        }else{
            echo $this->error_not_found();
        }

    }
    


}

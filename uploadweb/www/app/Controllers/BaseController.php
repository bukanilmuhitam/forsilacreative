<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use CodeIgniter\Controller;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

class BaseController extends Controller
{

	/**
	 * An array of helpers to be loaded automatically upon
	 * class instantiation. These helpers will be available
	 * to all other controllers that extend BaseController.
	 *
	 * @var array
	 */
	protected $helpers = ['form'];
	protected $CategoryModel;
	protected $subkategori;
	protected $product;
	protected $member;
	protected $cart;
	protected $order;
	/**
	 * Constructor.
	 */
	public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
	{
		// Do Not Edit This Line
		parent::initController($request, $response, $logger);

		//--------------------------------------------------------------------
		// Preload any models, libraries, etc, here.
		//--------------------------------------------------------------------
		// E.g.:
		// $this->session = \Config\Services::session();
		$this->CategoryModel = new \App\Models\CategoryModel();
		$this->subkategori = new \App\Models\SubcategoryModel();
		$this->product = new \App\Models\ProductModel();
		$this->member = new \App\Models\UserMember();
		$this->cart = new \App\Models\CartModel();
		$this->order = new \App\Models\OrderModel();

	}

	public function sendemail($subject , $to , $message){
		
	

		$mail = new PHPMailer(true);
 
        try {
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->Host       = 'smtp.googlemail.com';   
            $mail->SMTPAuth   = true;
            $mail->Username   = 'kokodev2021@gmail.com'; // silahkan ganti dengan alamat email Anda
            $mail->Password   = 'batosai21'; // silahkan ganti dengan password email Anda
            $mail->SMTPSecure = 'tls';
            $mail->Port       = 587;
 
            $mail->setFrom('kokodev2021@gmail.com', 'Forsila Admin'); // silahkan ganti dengan alamat email Anda
            $mail->addAddress($to);
            $mail->addReplyTo('kokodev2021@gmail.com', 'Forsila Admin'); // silahkan ganti dengan alamat email Anda
            // Content
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $message;
 
            $mail->send();
			return "success";
			
        } catch (Exception $e) {
            return "error";
        }

	}

	public function sendemailadmin($subject , $from , $message){
		$mail = new PHPMailer(true);
 
        try {
            $mail->SMTPDebug = SMTP::DEBUG_SERVER;
            $mail->isSMTP();
            $mail->Host       = 'smtp.googlemail.com';   
            $mail->SMTPAuth   = true;
            $mail->Username   = 'kokodev2021@gmail.com'; // silahkan ganti dengan alamat email Anda
            $mail->Password   = 'batosai21'; // silahkan ganti dengan password email Anda
            $mail->SMTPSecure = 'tls';
            $mail->Port       = 587;
 
            $mail->setFrom($from); // silahkan ganti dengan alamat email Anda
            $mail->addAddress('kokodev2021@gmail.com');
            $mail->addReplyTo('kokodev2021@gmail.com', 'Forsila Admin'); // silahkan ganti dengan alamat email Anda
            // Content
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->Body    = $message;
 
            $mail->send();
			return "success";
			
        } catch (Exception $e) {
            return "error";
        }
	}

	public function changenameimage($name){

        $pecah = explode('.' , $name);

        $getExt = $pecah[1];

        $random = rand ( 10000 , 99999 );

        $replacename = "image_".date('YmdHis').$random.".".$getExt;

        return $replacename;

	}

	public function changenamefile($name){

        $pecah = explode('.' , $name);

        $getExt = $pecah[1];

        $random = rand ( 10000 , 99999 );

        $replacename = "file_".date('YmdHis').$random.".".$getExt;

        return $replacename;

	}
	
	public function createusername($name){
		$getName = explode(' ' , $name);
		$random = rand ( 10000 , 99999 );
		return $getName[0].$random;
	}

	public function createtoken($name = "jhon"){
		$getName = explode(' ' , $name);
		$val = substr($getName[0] , 0 , 2);
		$random = rand ( 10000 , 99999 );
		return ucwords($val).date('YmdHis').$random;
	}

	public function converttexttoarray($text , $param = ','){
		return explode($param , $text);
	}

	public function error_not_found($message = 'Mohon maaf kami tidak bisa menampilkan yang kamu cari :('){
		$categories = $this->CategoryModel->getData();
		$subcategory = $this->subkategori->getData();

		$session = session();
		$username = session()->get('user_name');
        $cart = $this->cart->check_cart($username);
        if(empty($cart)){
				$countcart = 0;
		}else{
				$countcart = count($cart);
		}
	
		$data = [
			'title'=>'Halaman tidak ditemukan',
			'menus' => $categories,
			'submenus' => $subcategory,
			'message' => $message,
			'cart' => $cart,
			'countcart' => $countcart,
		];

		return view('404' , $data);
	}

	// GET PROVINCE
	public function getProvinsi(){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.rajaongkir.com/starter/province",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"key: 39a060ee39e03dd392913b4edf345d87"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response);

	}

	public function getKabupaten($idprovinsi){

		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.rajaongkir.com/starter/city?province=$idprovinsi",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "GET",
		CURLOPT_HTTPHEADER => array(
			"key: 39a060ee39e03dd392913b4edf345d87"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response);
		
	}

	public function showOngkir($asal , $destination){


		$curl = curl_init();

		curl_setopt_array($curl, array(
		CURLOPT_URL => "https://api.rajaongkir.com/starter/cost",
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => "",
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 30,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => "POST",
		CURLOPT_POSTFIELDS => "origin=$asal&destination=$destination&weight=500&courier=jne",
		CURLOPT_HTTPHEADER => array(
			"content-type: application/x-www-form-urlencoded",
			"key: 39a060ee39e03dd392913b4edf345d87"
		),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);

		curl_close($curl);

		return json_decode($response);
	}


}
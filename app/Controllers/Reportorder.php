<?php namespace App\Controllers;

class Reportorder extends BaseController
{
	public function index()
	{

            return view('adminpage/listorder/report');

    }


    public function handle_show_data_order(){

        $data = json_decode(file_get_contents("php://input"));
        
        $res = $this->order->report($data->start , $data->end);

       
     

        

        if(empty($res)){
            $blmproses = 0;
            $brgproses = 0;
            $shipping = 0;
            $complete = 0;

            $result = [
                'items' =>  0,
                'tBlmproses' => $blmproses, 
                'tProses' => $brgproses, 
                'tShipping' => $shipping, 
                'tComplete' => $complete, 
            ];

        }else{
            
            $blmproses = 0;
            $brgproses = 0;
            $shipping = 0;
            $complete = 0;

            foreach($res as $dt){

                if($dt['status_order'] == 0){
                    $blmproses += $dt['tpayment'];
                }
    
                if($dt['status_order'] == 1){
                    $brgproses += $dt['tpayment'];
                }
    
                if($dt['status_order'] == 2){
                    $shipping += $dt['tpayment'];
                }
    
                if($dt['status_order'] == 3){
                    $complete += $dt['tpayment'];
                }
    
            }

            $result = [
                'items' =>  $this->order->report($data->start , $data->end),
                'tBlmproses' => $blmproses, 
                'tProses' => $brgproses, 
                'tShipping' => $shipping, 
                'tComplete' => $complete, 
            ];
        }

        return  $this->response->setJSON($result);

    }

}
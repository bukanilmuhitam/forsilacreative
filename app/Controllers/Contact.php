<?php namespace App\Controllers;

class Contact extends BaseController
{
	public function index()
	{
       $email = $this->request->getPost('email');
       $message = $this->request->getPost('message');

       $subject = "Pesan Dari $email";
       $this->sendemailadmin($subject , $email , $message);
       session()->setFlashdata('msg', 'Berhasil Mengirim pesan, tunggu admin kami akan membalas email anda');
       return redirect()->to('/');
	}


}

<?php namespace App\Controllers;

class Forgotpassword extends BaseController
{

    public function index(){



        if(! $this->validate([]))
        {
            $data= [
				'title' => 'Forgot Password Member Forsila Creative',
                'validation' => $this->validator,
            ];
            return view('user/auth/forgot' , $data);
        }
    }

	public function prosesforgot(){


        $validated = $this->validate([
              
                'email' => 'required|valid_email',

            ],
            [
                'email' => [
                    'required' => 'Username wajib diisi',
                    'valid_email' => 'Email Anda Tidak Valid',
                ],
            ]
        );
        if ($validated == FALSE) {
            
            return $this->index();

        }else{
            $email = $this->request->getPost('email');
            $enskripsi = base64_encode($email);

            $cekuser = $this->member->getDataByEmail($email);

            if($cekuser == NULL){


                session()->setFlashdata("msg", "Email Kamu Tidak Terdaftar");
                return redirect()->to('/forgotpassword');

            }else{

                $pesan = "
                    Kamu baru saja meminta reset password. kamu bisa reset password melalui link dibawah ini.
                    <br /><br />
                        <a href='http://localhost:8080/forgotpassword/reset?kw=$enskripsi' target='_blank'>Reset Password Sekarang</a>
                    <br/><br />
                    abaikan email ini jika kamu merasa tidak melakukan reset password.
                    <br/><br />
                    Terimakasih.
                    <br /><br />
                    Salam hangat,
                    <br/><br />
                    Forsila Team
                ";

                $this->sendemail('Permintaan Reset Password' , $email , $pesan);
                session()->setFlashdata("msg", "Permintaan Reset Password Telah Dikirim ke email $email. silahkan cek email anda");
                return redirect()->to('/forgotpassword');
            }
            
          
        }
    }
    
    public function reset(){

        if(! $this->validate([])){
            $data= [
				'title' => 'Reset Password Member Forsila Creative',
                'validation' => $this->validator,
            ];
            return view('user/auth/reset' , $data);
        }

    }

    public function prosesreset(){

        $validated = $this->validate([
                'password' => 'required|min_length[8]|matches[repeat_password]',
            ],
            [
                'password' => [
                    'required' => 'Password wajib diisi',
                    'min_length' => 'Password Harus Lebih Dari 8 Karakter',
                    'matches' => 'Password kamu tidak sama',
                ],
            ]
         );
            if ($validated == FALSE) {
                
                return $this->reset();

            }else{

                $password = md5($this->request->getPost('password'));               
                $getemail = base64_decode($this->request->getPost('kw'));            
                $data = [
                    'password' => $password,
                ];

            

            //    dd($password);

                $res = $this->member->where('email' , $getemail)->set($data)->update();
                if($res == 'ok'){
                    session()->setFlashdata('msg', 'Berhasil reset password. Silahkan Login Kembali');
                    return redirect()->to('/login');
                }else{
                    session()->setFlashdata('error', 'Gagal Menambahkan data!');
                    return redirect()->to('/login');
                }
                
            
            }

    }
   


}

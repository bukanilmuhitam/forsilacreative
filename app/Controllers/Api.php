<?php namespace App\Controllers;

class Api extends BaseController
{
    public function index(){
        return "APi Console";
	}
	
	public function getCategory(){
		$data = $this->CategoryModel->getData();
		return $this->response->setJSON($data);
	}

	public function getSubcategory(){
		$getIdcategory = $this->request->getGet('id_category');
		$data = $this->subkategori->getByIdCategory($getIdcategory);
		return $this->response->setJSON($data);
	}
	
	public function getKabupatenrajaongkir(){

        $data = json_decode(file_get_contents("php://input"));

		$getprovinsi = explode('-' , $data->provinsi);
		$provinsiid = $getprovinsi[0];

		$datakab = $this->getKabupaten($provinsiid);
		$kab = $datakab->rajaongkir->results;
		return $this->response->setJSON($kab);

	}

	public function ongkoskirim(){

        $data = json_decode(file_get_contents("php://input"));
		$kotaasal = 278;
		$getkabkota = explode('-' , $data->kabkota);
		$kabkotaid = $getkabkota[0];
		$kurir = $data->kurir;

		$dataongkir = $this->showOngkir($kotaasal , $kabkotaid , $kurir);
		$ongkir = $dataongkir->rajaongkir->results;
		return $this->response->setJSON($ongkir[0]);

	}


}

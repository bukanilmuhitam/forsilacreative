<?php namespace App\Controllers;

class Produk extends BaseController
{
	public function index()
	{
		echo $this->error_not_found();
	}

	public function detail(){
		$uri = $this->request->uri->getSegments();

		if(empty($uri[2])){
			echo $this->error_not_found();
		}else{

			$session = session();
			$categories = $this->CategoryModel->getData();
            $subcategory = $this->subkategori->getData();
			$getID= $uri[2];
			$product = $this->product->gerProductByPermalink($getID);
			$DetailSubCategory = $this->subkategori->getData($product['sub_category']);

			// Produk dalam sub kategori serupa
			// $idSubcategory = $DetailSubCategory['id'];
			// $similiar_products = $this->product->getProductBySubcategory($idSubcategory);

			// Cart
			$username = session()->get('user_name');
			$cart = $this->cart->check_cart($username);
			if(empty($cart)){
				$countcart = 0;
			}else{
				$countcart = count($cart);
			}
			
			

			$data = [
                'title'=>'Percetakan Forsila Creative',
                'menus' => $categories,
                'submenus' => $subcategory,
				'product' => $product,
				'sub_kategori' => $DetailSubCategory['nama_subcategory'],
				'permalink_subkategori' => $DetailSubCategory['permalink'],
				'cart' => $cart,
			    'countcart' => $countcart,
			];
			// dd($similiar_products);
			
			return view('detailproduk' , $data);

		}
	}

	public function addcart(){
		$session = session();
		if(session()->get('login_user') == FALSE){
			$session->setFlashdata('msg', 'Maaf harus login terlebih dahulu');
            return redirect()->to('/login');
		}
		
		$username = session()->get('user_name');
		$idproduct = $this->request->getPost('idproduct');
		$permalink = $this->request->getPost('permalink');
		$qty = $this->request->getPost('qty');

		$DetailCart = $this->cart->getDataByUsername($username);

		
		$data = [
			'id_product' => $idproduct,
			'username' => $username,
			'qty' => $qty,
		];


		if(empty($DetailCart)){
			$res = $this->cart->saveData($data);
			if($res == 'ok'){
				session()->setFlashdata('msg', 'Berhasil Tambah Keranjang');
				return redirect()->to("/produk/detail/$permalink");
			}
		}else{
			$idproductcart = $DetailCart['id_product'];
			if($idproductcart == $idproduct){

				$qty_old = $DetailCart['qty'];
				$new_qty = $qty_old + $qty;
				$new_data = [
					'id_product' => $idproduct,
					'username' => $username,
					'qty' => $new_qty,
				];
				
				// dd($new_data);
				$res = $this->cart->updateByIdproduct($idproduct,$new_data);
				if($res == 'ok'){
					session()->setFlashdata('msg', 'Berhasil Tambah Keranjang');
					return redirect()->to("/produk/detail/$permalink");
				}
		

			}else{
				$res = $this->cart->saveData($data);
				if($res == 'ok'){
					session()->setFlashdata('msg', 'Berhasil Tambah Keranjang');
					return redirect()->to("/produk/detail/$permalink");
				}
			}
		}
		

	}

	public function hapuscart(){

		$idproduct = $this->request->getPost('idproduct');
		$res = $this->cart->deleteByIdproduct($idproduct);
		if($res == 'ok'){
			session()->setFlashdata('msg', 'Berhasil hapus produk dari keranjang');
			return redirect()->to("/order");
		}


	}

	public function updatequantity(){

		$idproduct = $this->request->getPost('idproduct');
		$qty = $this->request->getPost('qty');

		$data = [
			'qty' => $qty,
		];
		$res = $this->cart->updateByIdproduct($idproduct,$data);
		if($res == 'ok'){
			session()->setFlashdata('msg', 'Berhasil update quantity produk');
			return redirect()->to("/order");
		}


	}


}

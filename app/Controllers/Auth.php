<?php namespace App\Controllers;

class Auth extends BaseController
{

    public function index(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(! $this->validate([]))
        {
            $data= [
				'title' => 'Login Administrator forsila creative',
                'validation' => $this->validator,
            ];
            return view('adminpage/login' , $data);
        }
    }

	public function login()
	{

        $validated = $this->validate([
                'uname' => 'required',
                'password' => 'required',
            ],
            [
                'uname' => [
                    'required' => 'Username wajib diisi',
                ],
                'password' => [
                    'required' => 'Password wajib diisi',
                ],
            ]
        );
        if ($validated == FALSE) {
            
            return $this->index();

        }else{
            $session = session();
            $AdminUser = new \App\Models\AdminUser();
            $uname = $this->request->getPost('uname');
            $password = md5($this->request->getPost('password'));
            // die();
            $getmodel = $AdminUser->where('username' , $uname)->first();
            
            // var_dump($getmodel);
            // die();
            if($getmodel){
                $pass = $getmodel['password'];
                if($password == $pass){
                    $ses_data = [
                        'user_name'     => $getmodel['username'],
                        'logged_in'     => TRUE
                    ];
                    $session->set($ses_data);
                    return redirect()->to('/administrator');
                }else{
                    $session->setFlashdata('msg', 'Password tidak sesuai');
                    return redirect()->to('/auth');
                }

            }else{
                $session->setFlashdata('msg', 'Akun tidak ditemukan');
                return redirect()->to('/auth');
            }
        }
	}

    public function logout(){
        $session = session();
        $session->destroy();
        return redirect()->to('/');
    }

}

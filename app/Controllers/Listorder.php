<?php namespace App\Controllers;

class Listorder extends BaseController
{
	public function index(){
        
        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }


        $data= [
            'title' => 'List Order | Forsila Creative',
            'items' => $this->order->getData(),
            'products' => $this->product->getData(),
        ];
		return view('adminpage/listorder/index' , $data);
    }
    
    public function add(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        if(! $this->validate([])){ 
            $data= [
                'title' => 'Tambah Category Produk | Forsila Creative',
                'validation' => $this->validator,
            ];
            return view('adminpage/category/add' , $data);
        }
    }

    public function savedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
            'name_category' => 'required|max_length[30]',
        ],
        [
            'name_category' => [
                'required' => 'Nama Category harus diisi',
                'max_length' => 'Nama category tidak boleh lebih dari 30'
            ],
        ]
        );
        if ($validated == FALSE) {
            
            return $this->add();

        }else{
            
            $name_category = $this->request->getPost('name_category');
            $permalink = str_replace(' ' , '-' , $name_category);
           
            $data = [ 
                'nama_category' => $name_category,
                'permalink' => $permalink,
            ];
            
           $res = $this->CategoryModel->saveData($data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Menambahkan data!');
                return redirect()->to('/category');
            }else{
                session()->setFlashdata('error', 'Gagal Menambahkan data!');
                return redirect()->to('/category');
            }

        }
    }

    public function detail(){
        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $uri = $this->request->uri->getSegments();
        $getID= $uri[2];

        $Detail = $this->order->getDataByInvoiceNumber($getID);
        $data= [
            'title' => 'Detail Invoice',
            'detail' => $Detail,
            'products' => $this->product->getData(),
        ];
        return view('adminpage/listorder/detail' , $data);

    }

    public function setstatus(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $id =$this->request->getPost('id');
        $status_order = $this->request->getPost('status_order');
        $username = $this->request->getPost('username');
        $alasan = $this->request->getPost('alasan');
        $number_invoice = $this->request->getPost('number_invoice');
        $resiwaybill = $this->request->getPost('resiwaybill');
        $user = $this->member->getDataByUsername($username);
        $email = $user['email'];
        $namauser = $user['nama_user'];

        // dd($status_order);
        switch($status_order) {
            case 'proses' :
                $pesan = "
                Hallo $namauser, Orderderan kamu dengan nomor invoice : $number_invoice telah kami proses.
                <br />
                <br />
                Tunggu ya barangmu akan segera kami kirim
                <br /><br />
                Terimakasih.
                <br /><br />
                Salam hangat,
                <br/><br />
                Forsila Team
                ";
                $this->sendemail('Barang diproses' , $email , $pesan);
                $res = $this->order->updateStatusOrder($id , 1);
                if($res == 'ok'){
                    session()->setFlashdata('success', 'Berhasil Memproses Barang!');
                    return redirect()->to('/listorder');
                }else{
                    session()->setFlashdata('error', 'Gagal Memproses Barang!');
                    return redirect()->to('/listorder');
                }
                break;
            case 'shipping' :
                $pesan = "
                Hallo $namauser, Orderderan kamu dengan nomor invoice : $number_invoice telah kami kirim.
                <br />
                <br />
                Bersabarlah menunggu barang kamu.
                <br/><br />
                Berikut nomor resi atau waybill kamu $resiwaybill
                <br /><br />
                Terimakasih.
                <br /><br />
                Salam hangat,
                <br/><br />
                Forsila Team
                ";
                $this->sendemail('Barang diantar' , $email , $pesan);
                $res = $this->order->updateStatusOrder($id , 2);
                if($res == 'ok'){
                    session()->setFlashdata('success', 'Berhasil Shipping Barang!');
                    return redirect()->to('/listorder');
                }else{
                    session()->setFlashdata('error', 'Gagal Shipping Barang!');
                    return redirect()->to('/listorder');
                }
                break;
            case 'complete' :
                $pesan = "
                Hallo $namauser,
                <br />
                <br />
                Terimakasih telah berbelanja di forsila creative.
                <br /><br />
                Terimakasih.
                <br /><br />
                Salam hangat,
                <br/><br />
                Forsila Team
                ";
                $this->sendemail('Order Complete' , $email , $pesan);
                $res = $this->order->updateStatusOrder($id , 3);
                if($res == 'ok'){
                    session()->setFlashdata('success', 'Berhasil Menyelesaikan Orderan!');
                    return redirect()->to('/listorder');
                }else{
                    session()->setFlashdata('error', 'Gagal Menyelesaikan Orderan!');
                    return redirect()->to('/listorder');
                }
                break;
            default :
                $pesan = "
                Hallo $namauser,
                <br />
                <br />
                Mohon maaf barang atau pesanan kamu dibatalkan. alasannya adalah :
                <br /><br />
                $alasan
                <br /> <br />
                Mohon maaf atas ketidak nyamanan ini.
                <br /> <br />
                Terimakasih.
                <br /><br />
                Salam hangat,
                <br/><br />
                Forsila Team
                ";
                $this->sendemail('Order Complete' , $email , $pesan);
                $res = $this->order->updateStatusOrder($id , 4);
                if($res == 'ok'){
                    session()->setFlashdata('success', 'Berhasil Membatalkan Pesanan!');
                    return redirect()->to('/listorder');
                }else{
                    session()->setFlashdata('error', 'Gagal Membatalkan Pesanan!');
                    return redirect()->to('/listorder');
                }
        }

    }

    public function edit(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }
        
        if(! $this->validate([]))
        {
           
            $uri = $this->request->uri->getSegments();
            $getID= $uri[2];
            $getmodel = $this->CategoryModel->getData($getID);

            // var_dump($getmodel);
            // die();
            $data= [
                'title' => 'Edit Category',
                'item' => $getmodel,
                'validation' => $this->validator,
            ];
            return view('adminpage/category/edit' , $data);
        }

    }

    public function updatedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
            'name_category' => 'required|max_length[30]',
        ],
        [
            'name_category' => [
                'required' => 'Nama Category harus diisi',
                'max_length' => 'Nama category tidak boleh lebih dari 30'
            ],
        ]
        );
        if ($validated == FALSE) {
            
            return $this->add();

        }else{
            
            $id = $this->request->getPost('id');
            $name_category = $this->request->getPost('name_category');
            $permalink = str_replace(' ' , '-' , $name_category);
           
            $data = [ 
                'nama_category' => $name_category,
                'permalink' => $permalink,
            ];

            
           $res = $this->CategoryModel->updatedata( $id ,$data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Update data!');
                return redirect()->to('/category');
            }else{
                session()->setFlashdata('error', 'Gagal Update data!');
                return redirect()->to('/category');
            }

        }


    }

    public function delete(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $id = $this->request->getPost('id');
        $res = $this->CategoryModel->deletedata($id);

        if($res == 'ok'){
            session()->setFlashdata('success', 'Berhasil hapus data!');
            return redirect()->to('/category');
        }else{
            session()->setFlashdata('error', 'Gagal hapus data!');
            return redirect()->to('/category');
        }

    }


}

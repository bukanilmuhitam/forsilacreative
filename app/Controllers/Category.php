<?php namespace App\Controllers;

class Category extends BaseController
{
	public function index(){
        
        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }


        $data= [
            'title' => 'Categori Produk | Forsila Creative',
            'categories' => $this->CategoryModel->getData(),
        ];
		return view('adminpage/category/index' , $data);
    }
    
    public function add(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        if(! $this->validate([])){ 
            $data= [
                'title' => 'Tambah Category Produk | Forsila Creative',
                'validation' => $this->validator,
            ];
            return view('adminpage/category/add' , $data);
        }
    }

    public function savedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
            'name_category' => 'required|max_length[30]',
        ],
        [
            'name_category' => [
                'required' => 'Nama Category harus diisi',
                'max_length' => 'Nama category tidak boleh lebih dari 30'
            ],
        ]
        );
        if ($validated == FALSE) {
            
            return $this->add();

        }else{
            
            $name_category = $this->request->getPost('name_category');
            $permalink = str_replace(' ' , '-' , $name_category);
           
            $data = [ 
                'nama_category' => $name_category,
                'permalink' => $permalink,
            ];
            
           $res = $this->CategoryModel->saveData($data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Menambahkan data!');
                return redirect()->to('/category');
            }else{
                session()->setFlashdata('error', 'Gagal Menambahkan data!');
                return redirect()->to('/category');
            }

        }
    }

    public function edit(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }
        
        if(! $this->validate([]))
        {
           
            $uri = $this->request->uri->getSegments();
            $getID= $uri[2];
            $getmodel = $this->CategoryModel->getData($getID);

            // var_dump($getmodel);
            // die();
            $data= [
                'title' => 'Edit Category',
                'item' => $getmodel,
                'validation' => $this->validator,
            ];
            return view('adminpage/category/edit' , $data);
        }

    }

    public function updatedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
            'name_category' => 'required|max_length[30]',
        ],
        [
            'name_category' => [
                'required' => 'Nama Category harus diisi',
                'max_length' => 'Nama category tidak boleh lebih dari 30'
            ],
        ]
        );
        if ($validated == FALSE) {
            
            return $this->add();

        }else{
            
            $id = $this->request->getPost('id');
            $name_category = $this->request->getPost('name_category');
            $permalink = str_replace(' ' , '-' , $name_category);
           
            $data = [ 
                'nama_category' => $name_category,
                'permalink' => $permalink,
            ];

            
           $res = $this->CategoryModel->updatedata( $id ,$data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Update data!');
                return redirect()->to('/category');
            }else{
                session()->setFlashdata('error', 'Gagal Update data!');
                return redirect()->to('/category');
            }

        }


    }

    public function delete(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $id = $this->request->getPost('id');
        $res = $this->CategoryModel->deletedata($id);

        if($res == 'ok'){
            session()->setFlashdata('success', 'Berhasil hapus data!');
            return redirect()->to('/category');
        }else{
            session()->setFlashdata('error', 'Gagal hapus data!');
            return redirect()->to('/category');
        }

    }


}

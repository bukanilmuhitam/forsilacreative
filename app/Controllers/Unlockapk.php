<?php namespace App\Controllers;

class Unlockapk extends BaseController
{
	public function index()
	{
		return view('unlock');
    }
    
    public function unlock(){
        $session = session();
        $key = md5($this->request->getPost('key'));
        if($key == '8749ef714b2ccde06d480c3f07d3ff1c'){
            $ses_data = [
                'unlock'     => TRUE,
            ];
            $session->set($ses_data);
            return redirect()->to('/administrator');
        }
    }

	//--------------------------------------------------------------------

}

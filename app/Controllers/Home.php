<?php namespace App\Controllers;

class Home extends BaseController
{
	public function index()
	{
		$session = session();
		// This Get Menu Dynamic
		// Get menu by category
		$categories = $this->CategoryModel->getData();
		$subcategory = $this->subkategori->getData();

		// Cart
		$username = session()->get('user_name');
		$cart = $this->cart->check_cart($username);

		if(empty($cart)){
			$countcart = 0;
		}else{
			$countcart = count($cart);
		}

		// Get New Products
		$produk = $this->product->newProduct();
		$data = [
			'title'=>'Percetakan Forsila lain',
			'menus' => $categories,
			'submenus' => $subcategory,
			'products' => $produk,
			'cart' => $cart,
			'countcart' => $countcart,
		];
		
		return view('user/home/index' , $data);
	}


}

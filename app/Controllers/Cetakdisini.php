<?php namespace App\Controllers;

class Cetakdisini extends BaseController
{
	public function index()
	{
		$session = session();
		// This Get Menu Dynamic
		// Get menu by category
		$categories = $this->CategoryModel->getData();
		$subcategory = $this->subkategori->getData();

		// Cart
		$username = session()->get('user_name');
		$cart = $this->cart->check_cart($username);

		if(empty($cart)){
			$countcart = 0;
		}else{
			$countcart = count($cart);
		}

        if(! $this->validate([])){ 
		    // Get New Products
            $produk = $this->product->newProduct();
            $data = [
                'title'=>'Percetakan Forsila Creative',
                'menus' => $categories,
                'submenus' => $subcategory,
                'products' => $produk,
                'cart' => $cart,
                'countcart' => $countcart,
                'validation' => $this->validator,
            ];
		
		    return view('user/cetakdisini/cetakdisini' , $data);

        }
	}

    public function order(){

        $validated = $this->validate([
            'file_tugas' => 'uploaded[file_tugas]',
            'alamat_pengiriman' => 'required',
            'provinsi' => 'required',
            'kabupaten' => 'required',
            'kecamatan' => 'required',
            'kodepos' => 'required',
        ],
        [
            'alamat_pengiriman' => [
                'required' => 'Alamat pengiriman wajib diisi',
            ],
            'provinsi' => [
                'required' => 'Provinsi pengiriman wajib diisi',
            ],
            'kabupaten' => [
                'required' => 'Kabupaten pengiriman wajib diisi',
            ],
            'kecamatan' => [
                'required' => 'Kecamatan pengiriman wajib diisi',
            ],
            'kodepos' => [
                'required' => 'Kodepos pengiriman wajib diisi',
            ],
        ]
    );

    if ($validated == FALSE) {
        
        return $this->index();

    }else{

        $session = session();
        $number_invoice = "INVOICE-".rand( 100 , 2000);
        $username = $session->get('user_name');
        $alamat_pengiriman = $this->request->getPost('alamat_pengiriman');
        $provinsi = $this->request->getPost('provinsi');
        $kabupaten = $this->request->getPost('kabupaten');
        $kecamatan = $this->request->getPost('kecamatan');
        $kodepos = $this->request->getPost('kodepos');
        $detail_alamat = $this->request->getPost('detail_alamat');
        $lembar_file = $this->request->getPost('lembar_file');
        $ukuran_kertas = $this->request->getPost('ukuran_kertas');
        $warna_kertas = $this->request->getPost('warna_kertas');
        if($warna_kertas == 'Hitam'){
            $biayacetak = 100;
        }else{
            $biayacetak = 500;
        }
        $gambar_design = $this->request->getFile('file_tugas');
        $nama_gambar_design = $gambar_design->getName();
        $new_image = $this->changenamefile($nama_gambar_design);
        $tshipping = 10000;
        $tbelanja = $lembar_file * $biayacetak;
        $tpayment = $tshipping + $tbelanja;
        $catatantambahan = "Print kertas $ukuran_kertas sebanyak $lembar_file lembar dicetak dengan tinta $warna_kertas";
        $data= [
            'number_invoice' => $number_invoice,
            'username' => $username,
            'alamat_pengiriman' => $alamat_pengiriman,
            'provinsi' => $provinsi,
            'kabupaten' => $kabupaten,
            'kecamatan' => $kecamatan,
            'kode_pos' => $kodepos,
            'keterangan_tambahan_pengiriman' => $detail_alamat,
            'tshipping' => $tshipping,
            'tpayment' => $tpayment,
            'catatan_tambahan' => $catatantambahan,
            'status_order' => 0,
            'date_order' => date('Y-m-d'),
            'lampiran_file' => $new_image,
        ];
        // dd($data);
        $user = $this->member->getDataByUsername($username);
        $email = $user['email'];
        $namauser = $user['nama_user'];

        $totalpy = number_format($tpayment , 0 , '.' , '.');
        $pesan = "
        Hallo $namauser, Orderderan kamu dengan nomor invoice : $number_invoice telah kami terima.
        <br />
        <br />
        Kamu bisa melakukan pembayaran lewat transfer bank, melalui rekening dibawah ini :
        <br/>
        <br /> 
        BANK TESTING 1809-0909-2987
        <br />
        <br />
        Dengan Total Payment : Rp. $totalpy
        <br />
        <br />
        Lampirkan bukti pembayaran dengan membalas email ini agar barang kamu dapat kami proses.
        <br />
        <br />
        Jika ingin menambahkan lampiran file desain tambahan/lainnya hubungi customer service kami melalui whastapp 0865-9876-0909, dengan format #LAMPIRAN FILE DESIGN NOMOR INVOICE : $number_invoice
        <br /><br />
        Terimakasih.
        <br /><br />
        Salam hangat,
        <br/><br />
        Forsila Team
        ";
        $gambar_design->move(ROOTPATH .'public/upload/lampiran_design/' , $new_image);
        $res = $this->order->saveData($data);
        if($res == 'ok'){
            $this->sendemail('Order Sukses' , $email , $pesan);
            $this->cart->deleteByUsername($username);
            session()->setFlashdata('success', 'Sukses');
            session()->setFlashdata('totalBayar' , $tpayment);
            return redirect()->to("/order/orderfinish");
        }

    }


    }


}

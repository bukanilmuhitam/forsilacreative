<?php namespace App\Controllers;

class Listmember extends BaseController
{
	public function index()
	{
        
        

		$data = [
            'title'=>'List Member',
            'members' => $this->member->getData(),
        ];
        
		
		return view('adminpage/member/index' , $data);
	}

	public function add(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        if(! $this->validate([])){ 
            $data= [
                'title' => 'Tambah Member | Forsila Creative',
                'validation' => $this->validator,
            ];
            return view('adminpage/member/add' , $data);
        }
	}
	
	public function savedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
				'fullname' => 'required|max_length[30]',
				'no_hp' => 'max_length[13]|numeric',
				'email' => 'required|is_unique[user_member.email]|valid_email',
				'password' => 'required|min_length[8]|matches[ulangi_password]',
			],
			[
				'fullname' => [
					'required' => 'Username wajib diisi',
					'max_length' => 'Tidak lebih dari 30 karakter',
				],
				'no_hp' => [
					'max_length' => 'Tidak lebih dari 13 karakter',
					'numeric' => 'Hanya boleh diisi angka',
				],
				'email' => [
					'required' => 'Username wajib diisi',
					'is_unique' => 'Email anda telah digunakan',
					'valid_email' => 'Email Anda Tidak Valid',
				],
				'password' => [
					'required' => 'Password wajib diisi',
					'min_length' => 'Password Harus Lebih Dari 8 Karakter',
				],
			]
		);
		
        if ($validated == FALSE) {
            
            return $this->add();

        }else{
            
			$email = $this->request->getPost('email');
            $password = md5($this->request->getPost('password'));
            $fullname = $this->request->getPost('fullname');
            $no_hp = $this->request->getPost('no_hp');
            $username = $this->createusername($fullname);
            $data = [
                'email' => $email,
                'username' => $username,
                'password' => $password,
                'no_handphone' => $no_hp,
                'register_date' => date('Y-m-d H:i:s'),
                'status_user' => 1,
                'nama_user' => $fullname,
            ];
			// dd($data);
			
            
			$res = $this->member->saveData($data);
            if($res == 'ok'){
                 session()->setFlashdata('success', 'Berhasil Menambahkan Data!');
                 return redirect()->to('/listmember');
             }else{
                 session()->setFlashdata('error', 'Gagal Menambahkan data!');
                 return redirect()->to('/listmember');
             }

        }
	}
	
	public function edit(){
		if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        if(! $this->validate([])){ 

			$uri = $this->request->uri->getSegments();
			$getID= $uri[2];
			
			$getdata = $this->member->getData($getID);


			// dd($getdata);

            $data= [
                'title' => 'Edit Member',
				'validation' => $this->validator,
				'detail' => $getdata,
            ];
            return view('adminpage/member/edit' , $data);
        }
	}

	public function updatedata(){

		if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
				'fullname' => 'required|max_length[30]',
				'no_hp' => 'max_length[13]|numeric',
				'email' => 'required|valid_email',
			],
			[
				'fullname' => [
					'required' => 'Username wajib diisi',
					'max_length' => 'Tidak lebih dari 30 karakter',
				],
				'no_hp' => [
					'max_length' => 'Tidak lebih dari 13 karakter',
					'numeric' => 'Hanya boleh diisi angka',
				],
				'email' => [
					'required' => 'Username wajib diisi',
					'is_unique' => 'Email anda telah digunakan',
					'valid_email' => 'Email Anda Tidak Valid',
				],
			]
		);
		
        if ($validated == FALSE) {
            
            return $this->edit();

        }else{
            
			$id = $this->request->getPost('id');
			$email = $this->request->getPost('email');
            $fullname = $this->request->getPost('fullname');
            $no_hp = $this->request->getPost('no_hp');

            $data = [
                'email' => $email,
                'no_handphone' => $no_hp,
                'register_date' => date('Y-m-d H:i:s'),
                'status_user' => 1,
                'nama_user' => $fullname,
            ];
			// dd($data);
			
            
			$res = $this->member->updatedata($id , $data);
            if($res == 'ok'){
                 session()->setFlashdata('success', 'Berhasil Update Data!');
                 return redirect()->to('/listmember');
             }else{
                 session()->setFlashdata('error', 'Gagal Update data!');
                 return redirect()->to('/listmember');
             }

        }


	}

	public function nonaktifakun(){


			if(session()->get('unlock') == FALSE){
				// maka redirct ke halaman login
				return redirect()->to('/unlockapk'); 
			}

			if(session()->get('logged_in') == FALSE){
				return redirect()->to('/auth');
			}

			$id = $this->request->getPost('id');
			$username = $this->request->getPost('username');
			$alasan = $this->request->getPost('alasan');
			$user = $this->member->getDataByUsername($username);
			$email = $user['email'];
			$namauser = $user['nama_user'];


			$data = [
				'status_user' => 0,
			];
			// dd($data);
			
			
			$res = $this->member->updatedata($id , $data);
			if($res == 'ok'){

				$pesan = "
				Hallo $namauser,
				<br />
				<br />
				Mohon maaf akunmu kami non-aktifkan sementara dengan alasan :
				<br /><br />
				$alasan
				<br /> <br />
				bijaklah dalam belanja online.
				<br /> <br />
				Terimakasih.
				<br /><br />
				Salam hangat,
				<br/><br />
				Forsila Team
				";
				$this->sendemail('Akun Dimatikan' , $email , $pesan);

				session()->setFlashdata('success', 'Berhasil Mematikan Akun!');
				return redirect()->to('/listmember');
			}else{
				session()->setFlashdata('error', 'Gagal Mematikan Akun!');
				return redirect()->to('/listmember');
			}


	}

	public function aktifkanakun(){


			if(session()->get('unlock') == FALSE){
				// maka redirct ke halaman login
				return redirect()->to('/unlockapk'); 
			}

			if(session()->get('logged_in') == FALSE){
				return redirect()->to('/auth');
			}

			$id = $this->request->getPost('id');
			$username = $this->request->getPost('username');
			$user = $this->member->getDataByUsername($username);
			$email = $user['email'];
			$namauser = $user['nama_user'];


			$data = [
				'status_user' => 1,
			];
			// dd($data);
			
			
			$res = $this->member->updatedata($id , $data);
			if($res == 'ok'){

				$pesan = "
				Hallo $namauser,
				<br />
				<br />
				Akunmu telah kami aktifkan kembali.
				<br /> <br />
				Tetapi ikuti aturan yang sudah diatur oleh sistem kami.
				<br /> <br />
				Terimakasih.
				<br /><br />
				Salam hangat,
				<br/><br />
				Forsila Team
				";
				$this->sendemail('Akun Diaktifkan' , $email , $pesan);

				session()->setFlashdata('success', 'Berhasil Mematikan Akun!');
				return redirect()->to('/listmember');
			}else{
				session()->setFlashdata('error', 'Gagal Mematikan Akun!');
				return redirect()->to('/listmember');
			}


	}

	public function delete(){

		if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

		$id = $this->request->getPost('id');
        $username = $this->request->getPost('username');
		$alasan_hapus = $this->request->getPost('alasan_hapus');
		$user = $this->member->getDataByUsername($username);
        $email = $user['email'];
		$namauser = $user['nama_user'];


		$pesan = "
			Hallo $namauser,
			<br />
			<br />
			Mohon maaf akunmu kami hapus dengan alasan :
			<br /><br />
			$alasan_hapus
			<br /> <br />
			bijaklah dalam belanja online.
			<br /> <br />
			Terimakasih.
			<br /><br />
			Salam hangat,
			<br/><br />
			Forsila Team
			";
		$this->sendemail('Akun di delete' , $email , $pesan);

		
        $res = $this->member->deletedata($id);
        if($res == 'ok'){

            session()->setFlashdata('success', 'Berhasil hapus data!');
            return redirect()->to('/listmember');
        }else{
            session()->setFlashdata('error', 'Gagal hapus data!');
            return redirect()->to('/listmember');
        }

	}


}

<?php namespace App\Controllers;

class Listproduk extends BaseController
{
	public function index()
	{

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $data = [
            'title' => 'List Produk',
            'products' => $this->product->getData(),
        ];
        

		return view('adminpage/listproduk/index' , $data);
    }
    
    public function add(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        if(! $this->validate([])){ 
            $data= [
                'title' => 'Tambah List Produk | Forsila Creative',
                'validation' => $this->validator,
            ];

            return view('adminpage/listproduk/add' , $data);
        }

    }

    public function savedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
            'nama_produk' => 'required',
            'harga_produk' => 'required|numeric',
            'gambar_produk' => 'uploaded[gambar_produk]|mime_in[gambar_produk,image/jpg,image/jpeg,image/gif,image/png]',
        ],
        [
            'nama_produk' => [
                'required' => 'Nama produk harus diisi',
            ],
            'harga_produk' => [
                'required' => 'Harga produk harus diisi',
                'numeric' => 'Hanya dapat diisi numeric',
            ],
            'gambar_produk' => [

                'uploaded' => 'Gambar tidak boleh kosong',

                'mime_in' => 'File tidak didukung. file harus (JPG , JPEG , PNG )',

            ],
        ]
        );

        if ($validated == FALSE) {
            
            return $this->add();

        }else{
            
            $nama_produk = $this->request->getPost('nama_produk');
            $permalink = str_replace(' ' , '-' , $nama_produk);
            if($this->request->getPost('panjang') !== ''){
                $ukuran = $this->request->getPost('panjang') .' x '.$this->request->getPost('lebar');
            }else{
                $ukuran = '-';
            }
            // changenameimage
            $gambar_produk = $this->request->getFile('gambar_produk');
            $nama_gambar_produk = $gambar_produk->getName();
            $new_image = $this->changenameimage($nama_gambar_produk);
            $gambar_produk->move(ROOTPATH .'public/upload/product/' , $new_image);
            $data = [ 
                'sub_category' => $this->request->getPost('subkategori'),
                'nama_produk' => $nama_produk,
                'materials' => $this->request->getPost('materials'),
                'kuantitas' => $this->request->getPost('kuantitas_produk'),
                'estimasi_selesai_produksi' => $this->request->getPost('estimasi_selesai_produksi'),
                'gambar_produk' => $new_image,
                'deskripsi_produk' => $this->request->getPost('deskripsi_produk'),
                'type_ukuran' => 0,
                'ukuran_produk' => $ukuran,
                'harga_default_pcs' => $this->request->getPost('harga_produk'),
                'permalink_product' => $permalink,
                'berat_product' => $this->request->getPost('berat_produk'),
            ];
            
           
            
         
           $res = $this->product->saveData($data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Menambahkan data!');
                return redirect()->to('/listproduk');
            }else{
                session()->setFlashdata('error', 'Gagal Menambahkan data!');
                return redirect()->to('/listproduk');
            }

        }


    }

    public function edit(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        if(! $this->validate([])){ 

            $uri = $this->request->uri->getSegments();
            $getID= $uri[2];

            $getData = $this->product->getData($getID);

            // dd($getData);

            $data= [
                'title' => 'Edit List Produk | Forsila Creative',
                'validation' => $this->validator,
                'detail' => $getData,
            ];

            return view('adminpage/listproduk/edit' , $data);
        }

    }

    public function updatedata(){

        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $validated = $this->validate([
            'nama_produk' => 'required',
            'harga_produk' => 'required|numeric',
            'gambar_produk' => 'mime_in[gambar_produk,image/jpg,image/jpeg,image/gif,image/png]',
        ],
        [
            'nama_produk' => [
                'required' => 'Nama produk harus diisi',
            ],
            'harga_produk' => [
                'required' => 'Harga produk harus diisi',
                'numeric' => 'Hanya dapat diisi numeric',
            ],
            'gambar_produk' => [
                'mime_in' => 'File tidak didukung. file harus (JPG , JPEG , PNG )',
            ],
        ]
        );

        if ($validated == FALSE) {
            
            return $this->edit();

        }else{
            
            $id = $this->request->getPost('id');
            $nama_produk = $this->request->getPost('nama_produk');
            $permalink = str_replace(' ' , '-' , $nama_produk);
            $ukuran = $this->request->getPost('ukuran');
            // changenameimage
            $gambar_produk = $this->request->getFile('gambar_produk');

            // GET OLD IMAGE
            $detail = $this->product->getData($id);
            $old_image = $detail['gambar_produk'];
          
            if(!empty($this->request->getPost('kategori'))){
                if($gambar_produk != ''){

                    $path = $_SERVER['DOCUMENT_ROOT'].'/upload/product/'.$old_image;
                    unlink($path);
                    $nama_gambar_produk = $gambar_produk->getName();
                    $new_image = $this->changenameimage($nama_gambar_produk);
                    $gambar_produk->move(ROOTPATH .'public/upload/product/' , $new_image);
        

                    $data = [ 
                        'sub_category' => $this->request->getPost('subkategori'),
                        'nama_produk' => $nama_produk,
                        'materials' => $this->request->getPost('materials'),
                        'kuantitas' => $this->request->getPost('kuantitas_produk'),
                        'estimasi_selesai_produksi' => $this->request->getPost('estimasi_selesai_produksi'),
                        'gambar_produk' => $new_image,
                        'deskripsi_produk' => $this->request->getPost('deskripsi_produk'),
                        'type_ukuran' => 0,
                        'ukuran_produk' => $ukuran,
                        'harga_default_pcs' => $this->request->getPost('harga_produk'),
                        'permalink_product' => $permalink,
                        'berat_product' => $this->request->getPost('berat_produk'),
                    ];

                }else{

                    $data = [ 
                        'sub_category' => $this->request->getPost('subkategori'),
                        'nama_produk' => $nama_produk,
                        'materials' => $this->request->getPost('materials'),
                        'kuantitas' => $this->request->getPost('kuantitas_produk'),
                        'estimasi_selesai_produksi' => $this->request->getPost('estimasi_selesai_produksi'),
                        'deskripsi_produk' => $this->request->getPost('deskripsi_produk'),
                        'type_ukuran' => 0,
                        'ukuran_produk' => $ukuran,
                        'harga_default_pcs' => $this->request->getPost('harga_produk'),
                        'permalink_product' => $permalink,
                        'berat_product' => $this->request->getPost('berat_produk'),
                    ];
                }
            }else{

                if($gambar_produk != ''){

                    $path = $_SERVER['DOCUMENT_ROOT'].'/upload/product/'.$old_image ;
                    unlink($path);
                    $nama_gambar_produk = $gambar_produk->getName();
                    $new_image = $this->changenameimage($nama_gambar_produk);
                    $gambar_produk->move(ROOTPATH .'public/upload/product/' , $new_image);
        

                    $data = [ 
                        'nama_produk' => $nama_produk,
                        'materials' => $this->request->getPost('materials'),
                        'kuantitas' => $this->request->getPost('kuantitas_produk'),
                        'estimasi_selesai_produksi' => $this->request->getPost('estimasi_selesai_produksi'),
                        'gambar_produk' => $new_image,
                        'deskripsi_produk' => $this->request->getPost('deskripsi_produk'),
                        'type_ukuran' => 0,
                        'ukuran_produk' => $ukuran,
                        'harga_default_pcs' => $this->request->getPost('harga_produk'),
                        'permalink_product' => $permalink,
                        'berat_product' => $this->request->getPost('berat_produk'),
                    ];

                }else{

                    $data = [ 
                        'nama_produk' => $nama_produk,
                        'materials' => $this->request->getPost('materials'),
                        'kuantitas' => $this->request->getPost('kuantitas_produk'),
                        'estimasi_selesai_produksi' => $this->request->getPost('estimasi_selesai_produksi'),
                        'deskripsi_produk' => $this->request->getPost('deskripsi_produk'),
                        'type_ukuran' => 0,
                        'ukuran_produk' => $ukuran,
                        'harga_default_pcs' => $this->request->getPost('harga_produk'),
                        'permalink_product' => $permalink,
                        'berat_product' => $this->request->getPost('berat_produk'),
                    ];
                }

            }
            
            // dd($data);
           
            
         
           $res = $this->product->updatedata( $id , $data);
           if($res == 'ok'){
                session()->setFlashdata('success', 'Berhasil Update data!');
                return redirect()->to('/listproduk');
            }else{
                session()->setFlashdata('error', 'Gagal Update data!');
                return redirect()->to('/listproduk');
            }

        }


    }

    public function delete(){

        
        if(session()->get('unlock') == FALSE){
            // maka redirct ke halaman login
            return redirect()->to('/unlockapk'); 
        }

        if(session()->get('logged_in') == FALSE){
            return redirect()->to('/auth');
        }

        $id = $this->request->getPost('id');

        // DELETE IMAGE
        // GET OLD IMAGE
        $detail = $this->product->getData($id);
        $old_image = $detail['gambar_produk'];
        $path = $_SERVER['DOCUMENT_ROOT'].'/upload/product/'.$old_image;
        unlink($path);
    

        $res = $this->product->deletedata($id);

        if($res == 'ok'){
            session()->setFlashdata('success', 'Berhasil hapus data!');
            return redirect()->to('/listproduk');
        }else{
            session()->setFlashdata('error', 'Gagal hapus data!');
            return redirect()->to('/listproduk');
        }


    }

    public function detail(){

        
        $uri = $this->request->uri->getSegments();
        $getID= $uri[2];

        $getdata = $this->product->gerProductByPermalink($getID);

        $DetailSubCategory = $this->subkategori->getData($getdata['sub_category']);

        $data= [
            'title' => 'Detail Order',
            'detail' => $getdata,
            'sub_kategori' => $DetailSubCategory['nama_subcategory'],
        ];

        return view('adminpage/listproduk/detail' , $data);

    }


}

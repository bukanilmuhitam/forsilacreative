<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Product</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Product</p>
            </li>
            <li>
                <p class="font-light text-gray-400">List produk</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>List produk</h3>
            <a href="/listproduk/add" class="py-1 px-4 bg-blue-600 text-xs ml-auto rounded text-white"><i
                    class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="card-body">

        <!-- Table -->
        <table class="w-full">
            <thead>
                <tr>
                    <!-- <th style="width:5%;" class="py-2 px-2 text-sm border">#</th> -->
                    <th class="py-2 px-2 text-sm border">Produk</th>
                    <!-- <th style="width:20%;"  class="py-2 px-2 text-sm border">#</th> -->
                </tr>
            </thead>
            <tbody>
                <?php 
                    $no = 1;
                    foreach($products as $product){
                ?>
                 <tr>
                    <!-- <td class="py-2 px-2 text-sm border text-center"><?=$no++?>. </td> -->
                    <td class="py-2 px-2 text-sm border">
                        <div class="w-full flex space-x-4">
                            <div class="w-1/5 h-48">
                                <img src="<?=base_url()?>/upload/product/<?=$product['gambar_produk'];?>" alt="<?=$product['nama_produk'];?>">
                            </div>
                            <di class="flex-1">
                               <div class="flex">
                                    <h3 class="capitalize text-2xl"></h3>
                                    <a href="/listproduk/detail/<?=$product['permalink_product'];?>" class="text-lg font-semibold underline text-blue-600">#<?=$product['nama_produk']?></a>
                                    <div class="flex ml-auto space-x-4">
                                        <a href="/listproduk/edit/<?=$product['id'];?>" title="Edit Data" class="py-1 px-4 bg-blue-600 text-white modal-btn rounded"><i class="fa fa-edit"></i></a>    
                                        <button data-id="konfirmasidelete<?=$product['id'];?>" title="delete"
                                            class="py-1 px-4 bg-red-600 text-white text-xs modal-btn rounded"><i
                                                class="fa fa-trash"></i></button>&nbsp;
                                    </div>
                               </div>
                               <p>material : <?=$product['materials'];?></p>
                               <p>ukuran : <?=$product['ukuran_produk'];?> cm</p>
                               <p class="mt-6 text-lg font-semibold">Rp. <?=number_format($product['harga_default_pcs'] , 0 , '.' , '.')?></p>
                            </div>
                        </div>
                    </td>
                </tr>

                
                <div id="konfirmasidelete<?=$product['id'];?>" class="modal">
                    <div class="bg-white max-w-screen-sm mx-auto">
                        <div class="border border-gray-200">
                            <div class="p-4 flex justify-between">
                                <h3 class="font-semibold flex items-center">Konfirmasi</h3>
                                <span class="close font-bold text-2xl text-gray-300 flex items-center">&times;</span>
                            </div>
                        </div>
                        <form action="/listproduk/delete" method="post">
                            <div class="p-6">
                                Apakah anda yakin ingin menghapus data?
                                <input type="hidden" name="id" value="<?=$product['id'];?>">
                            </div>
                            <div class="border border-gray-200">
                                <div class="p-4">
                                    <button type="submit" class="btn btn-success">Ya, saya ingin menghapus data</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php }?>
            </tbody>
        </table>
        <!-- End Table -->

    </div>
</div>
<?= $this->endSection() ?>

<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Product</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Product</p>
            </li>
            <li>
                <p class="font-light text-gray-400">Sub category</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Sub Category product</h3>
            <!-- <a href="/subcategory/add" class="py-1 px-4 bg-blue-600 text-xs ml-auto rounded text-white"><i
                    class="fa fa-plus"></i></a> -->
        </div>
    </div>
    <div class="card-body">

        <!-- Table -->
        <table class="w-full">
            <thead>
                <tr>
                    <th style="width:5%;" class="py-2 px-2 text-sm border">#</th>
                    <th class="py-2 px-2 text-sm border">Nama categori</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if(!empty($items)){
                        $no= 1;
                        
                    

                        foreach($items as $item){
                            
                ?>
                <tr>
                    <td class="py-2 px-2 text-sm border text-center"><?=$no++?>. </td>
                    <td class="py-2 px-2 text-sm border">
                        <?php 
                            foreach($categories as $category){
                                if($item['id_category'] == $category['id']){
                              
                                 
                            
                        ?>
                            <a class="text-blue-600 hover:text-red-600" href="/subcategory/show/<?=$category['id']?>"><?=$category['nama_category'];?></a>
                        <?php }}?>
                    </td>
                </tr>
                
                <?php }}else{?>
                <tr>
                    <td colspan="4" class="py-2 px-2 text-sm border  font-light text-center">Tidak ada data ditemukan
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <!-- End Table -->

    </div>
</div>
<?= $this->endSection() ?>
<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Order</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Order</p>
            </li>
            <li>
                <p class="font-light text-gray-400">List Order</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<div class="my-4 <?=!empty(session()->getFlashdata('success')) ? 'block' : 'hidden'?>">
    <div class="bg-green-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-check"></i>
            &nbsp;
            <span>Success!</span>
        </h3>
        <p class="mt-2 text-md">
            <?=session()->getFlashdata('success');?>
        </p>
    </div>
</div>

<div class="my-4 <?=!empty(session()->getFlashdata('error')) ? 'block' : 'hidden'?>">
    <div class="bg-red-500 text-white py-3 px-8 rounded-md">
        <h3 class="text-2xl">
            <i class="fas fa-times"></i>
            &nbsp;
            <span>Error!</span>
        </h3>
        <p class="mt-2 text-md">
            <?php echo session()->getFlashdata('error');?>
        </p>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>List Order</h3>
            <a href="/listorder/add" class="py-1 px-4 bg-blue-600 text-xs ml-auto rounded text-white"><i
                    class="fa fa-plus"></i></a>
        </div>
    </div>
    <div class="card-body">

        <!-- Table -->
        <table class="w-full">
            <thead>
                <tr>
                    <th style="width:5%;" class="py-2 px-2 text-sm border">#</th>
                    <th class="py-2 px-2 text-sm border">Detail Order</th>
                    <th class="py-2 px-2 text-sm border">Total Payment</th>
                    <th style="width:20%;"  class="py-2 px-2 text-sm border">#</th>
                </tr>
            </thead>
            <tbody>
                <?php 
                    if(!empty($items)){
                        $no= 1;
                        foreach($items as $item){

                            if($item['status_order'] == 0){
                               $status = 'Belum diproses';
                               $color = 'text-black';
                               $statbutton = 'proses';
                            }else if($item['status_order'] == 1){
                                $status = 'Barang diproses';
                                $color = 'text-yellow-600'; 
                                $statbutton = 'shipping';   
                            }else if($item['status_order'] == 2){
                                $status = 'Barang diantar';
                                $color = 'text-blue-600';    
                                $statbutton = 'complete';
                            }else if($item['status_order'] == 3){
                                $status = 'Order Complete';
                                $color = 'text-green-600';    
                                $statbutton = 'batal';
                            }else{
                                $status = 'Pesanan Batal';
                                $color = 'text-red-600';    
                                $statbutton = 'proses';
                            }
                ?>
                <tr>
                    <td class="py-2 px-2 text-sm border text-center"><?=$no++?>. </td>
                    <td class="py-2 px-2 text-sm border">
                        <a href="/listorder/detail/<?=$item['number_invoice'];?>" class="text-lg font-semibold underline text-blue-600">#<?=$item['number_invoice'];?></a>
                        <div class="mt-4">
                            <p>Date Order : <?=$item['date_order']?></p>
                            <p>Username : <?=$item['username'];?></p>
                        </div>
                        <h6 class="<?=$color?>">
                           Status Order : <span class="font-semibold"><?=$status?></span>
                        </h6>
                        <h6>
                            Lampiran File : <?php if($item['lampiran_file'] != ''){ ?><a href="<?=base_url()?>/upload/lampiran_design/<?=$item['lampiran_file'];?>" class="text-blue-500 underline" download>downloand</a><?php }else { echo '-';}?>
                        </h6>
                    </td>
                    <td class="py-2 px-2 border">
                         <p class="text-2xl font-semibold">Rp. <?=number_format($item['tpayment'] , 0 , '.' , '.');?></p>
                    </td>
                    <td class="py-2 px-2 text-sm border text-center">
                            <?php 
                                if($statbutton == 'proses'){
                            ?>
                                <button data-id="orderproses<?=$item['id'];?>" title="delete"
                                class="py-1 px-4 bg-black text-white text-xs modal-btn rounded">Proses</button>
                            <?php }?>
                            <?php 
                                if($statbutton == 'shipping'){
                            ?>
                                <button data-id="ordershipping<?=$item['id'];?>" title="delete"
                                class="py-1 px-4 bg-yellow-600 text-white text-xs modal-btn rounded">Shipping Proses</button>
                            <?php }?>
                            <?php 
                                if($statbutton == 'complete'){
                            ?>
                                <button data-id="ordercomplete<?=$item['id'];?>" title="delete"
                                class="py-1 px-4 bg-green-600 text-white text-xs modal-btn rounded">Selesaikan Order</button>
                            <?php }?>
                            <?php 
                                if($statbutton == 'batal'){
                            ?>
                                <button data-id="orderbatal<?=$item['id'];?>" title="delete"
                                class="py-1 px-4 bg-red-600 text-white text-xs modal-btn rounded">Batalkan Order</button>
                            <?php }?>
                    </td>
                  
                </tr>
                <div id="orderproses<?=$item['id'];?>" class="modal">
                    <div class="bg-white max-w-screen-sm mx-auto">
                        <div class="border border-gray-200">
                            <div class="p-4 flex justify-between">
                                <h3 class="font-semibold flex items-center">Konfirmasi</h3>
                                <span class="close font-bold text-2xl text-gray-300 flex items-center">&times;</span>
                            </div>
                        </div>
                        <form action="/listorder/setstatus" method="post">
                            <div class="p-6">
                                Apakah  anda yakin ingin menerima orderan ini?
                                <input type="hidden" name="id" value="<?=$item['id'];?>">
                                <input type="hidden" name="number_invoice" value="<?=$item['number_invoice'];?>">
                                <input type="hidden" name="status_order" value="proses">
                                <input type="hidden" name="username" value="<?=$item['username'];?>">
                            </div>
                            <div class="border border-gray-200">
                                <div class="p-4">
                                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="ordershipping<?=$item['id'];?>" class="modal">
                    <div class="bg-white max-w-screen-sm mx-auto">
                        <div class="border border-gray-200">
                            <div class="p-4 flex justify-between">
                                <h3 class="font-semibold flex items-center">Konfirmasi</h3>
                                <span class="close font-bold text-2xl text-gray-300 flex items-center">&times;</span>
                            </div>
                        </div>
                        <form action="/listorder/setstatus" method="post">
                            <div class="p-6">
                                Apakah  anda yakin ingin melanjutkan proses pengantaran barang?
                                <input type="hidden" name="id" value="<?=$item['id'];?>">
                                <input type="hidden" name="number_invoice" value="<?=$item['number_invoice'];?>">
                                <input type="hidden" name="status_order" value="shipping">
                                <input type="hidden" name="username" value="<?=$item['username'];?>">
                                <div class="mt-4 flex flex-col">
                                      <label for="">Nomor Resi/Waybill</label>
                                      <input type="text" name="resiwaybill" class="border py-2 px-4 rounded w-full focus:outline-none focus:border-blue-600" placeholder="Masukan nomor resi atau waybill"> 
                                </div>
                            </div>
                            <div class="border border-gray-200">
                                <div class="p-4">
                                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="ordercomplete<?=$item['id'];?>" class="modal">
                    <div class="bg-white max-w-screen-sm mx-auto">
                        <div class="border border-gray-200">
                            <div class="p-4 flex justify-between">
                                <h3 class="font-semibold flex items-center">Konfirmasi</h3>
                                <span class="close font-bold text-2xl text-gray-300 flex items-center">&times;</span>
                            </div>
                        </div>
                        <form action="/listorder/setstatus" method="post">
                            <div class="p-6">
                                Apakah  anda yakin ingin menyelesaikan orderan?
                                <input type="hidden" name="id" value="<?=$item['id'];?>">
                                <input type="hidden" name="number_invoice" value="<?=$item['number_invoice'];?>">
                                <input type="hidden" name="status_order" value="complete">
                                <input type="hidden" name="username" value="<?=$item['username'];?>">
                            </div>
                            <div class="border border-gray-200">
                                <div class="p-4">
                                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="orderbatal<?=$item['id'];?>" class="modal">
                    <div class="bg-white max-w-screen-sm mx-auto">
                        <div class="border border-gray-200">
                            <div class="p-4 flex justify-between">
                                <h3 class="font-semibold flex items-center">Konfirmasi</h3>
                                <span class="close font-bold text-2xl text-gray-300 flex items-center">&times;</span>
                            </div>
                        </div>
                        <form action="/listorder/setstatus" method="post">
                            <div class="p-6">
                                Apakah  anda yakin ingin membatalkan Pesanan ini?
                                <input type="hidden" name="id" value="<?=$item['id'];?>">
                                <input type="hidden" name="number_invoice" value="<?=$item['number_invoice'];?>">
                                <input type="hidden" name="status_order" value="batal">
                                <div class="mt-4">
                                    <div class="flex flex-col">
                                        <label for="">Sebutkan alsan untuk pembeli</label>
                                        <input type="text" name="alasan" class="border py-2 px-4 rounded focus:outline-none focus:border-blue-600" placeholder="Ketik alasan">
                                    </div>
                                </div>
                                <input type="hidden" name="username" value="<?=$item['username'];?>">
                            </div>
                            <div class="border border-gray-200">
                                <div class="p-4">
                                    <button type="submit" class="btn btn-success">Lanjutkan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <?php }}else{?>
                <tr>
                    <td colspan="3" class="py-2 px-2 text-sm border  font-light text-center">Tidak ada data ditemukan
                    </td>
                </tr>
                <?php }?>
            </tbody>
        </table>
        <!-- End Table -->

    </div>
</div>
<?= $this->endSection() ?>
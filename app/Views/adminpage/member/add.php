<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Member</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Member</p>
            </li>
            <li>
                <a href="/listmember" class="font-light">List Member</a>
            </li>
            <li>
                <p class="font-light text-gray-400">Tambah Data</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>
<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Tambah Data</h3>
        </div>
    </div>
    <div class="card-body">
        <form action="/listmember/savedata" method="post">
            <div class="flex flex-col mb-6">
                <label for="" class="text-gray-400">Nama Lengkap</label>
                <input type="text" name="fullname"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('fullname'))){ echo "invalid";}?>"
                    placeholder="Masukan fullname" value="<?php echo set_value('fullname'); ?>">
                <?php 
                            if(!empty($validation->getError('fullname'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('fullname')?></small>
                <?php }?>
            </div>
            <div class="flex flex-col mb-6">
                <label for="" class="text-gray-400">Email</label>
                <input type="text" name="email"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('email'))){ echo "invalid";}?>"
                    placeholder="Masukan email" value="<?php echo set_value('email'); ?>">
                <?php 
                            if(!empty($validation->getError('email'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('email')?></small>
                <?php }?>
            </div>
            <div class="flex flex-col mb-6">
                <label for="" class="text-gray-400">No. Handphone (optional)</label>
                <input type="text" name="no_hp"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('no_hp'))){ echo "invalid";}?>"
                    placeholder="Masukan nomor handphone" value="<?php echo set_value('no_hp'); ?>">
                <?php 
                            if(!empty($validation->getError('no_hp'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('no_hp')?></small>
                <?php }?>
            </div>
            <div class="flex flex-col mb-6">
                <label for="" class="text-gray-400">Password</label>
                <input type="password" name="password"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('password'))){ echo "invalid";}?>"
                    placeholder="Masukan password" value="<?php echo set_value('password'); ?>">
                <?php 
                            if(!empty($validation->getError('password'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('password')?></small>
                <?php }?>
            </div>
            <div class="flex flex-col mb-6">
                <label for="" class="text-gray-400">Ulangi Password</label>
                <input type="password" name="ulangi_password"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('ulangi_password'))){ echo "invalid";}?>"
                    placeholder="Ulangi password" value="<?php echo set_value('ulangi_password'); ?>">
                <?php 
                            if(!empty($validation->getError('ulangi_password'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('ulangi_password')?></small>
                <?php }?>
            </div>
            <div class="my-4 flex space-x-2">
                    <a href="/listmember" class="btn btn-secondary">Cancel</a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
<?= $this->endSection() ?>
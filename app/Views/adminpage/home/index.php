<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Dashboard</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <a href="#" class="font-light">Home</a>
            </li>
            <li>
                <p class="font-light text-gray-400">Dashboard</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>

<!-- Widget -->
<div class="bg-white p-6">
    <h1 class="text-2xl">Selamat datang pada halaman administrator</h1>
</div>
<!-- Widget -->
<?= $this->endSection() ?>
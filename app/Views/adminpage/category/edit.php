<?= $this->extend('layout/layout_admin') ?>


<?= $this->section('breadchumb') ?>
<!-- Breadchumb -->
<div class="bg-white sticky top-0">
    <div class="py-6 px-10 flex">
        <h3 class="text-2xl">Product</h3>
        <ul class="flex ml-auto space-x-4">
            <li>
                <p class="font-light">Product</p>
            </li>
            <li>
                <a href="/category" class="font-light">Category</a>
            </li>
            <li>
                <p class="font-light text-gray-400">Edit Data</p>
            </li>
        </ul>
    </div>
</div>
<!-- End Breadchumb -->
<?= $this->endSection()?>


<?= $this->section('content') ?>
<div class="card">
    <div class="card-header">
        <div class="title-header">
            <h3>Edit Data</h3>
        </div>
    </div>
    <div class="card-body">
        <form action="/category/updatedata" method="post">
            <div class="flex flex-col">
                <label for="" class="text-gray-400">Nama Category</label>
                <input type="text" name="name_category"
                    class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('name_category'))){ echo "invalid";}?>"
                    placeholder="Nama category" value="<?=$item['nama_category'];?>">
                <input type="hidden" name="id" value="<?=$item['id'];?>">
                <?php 
                            if(!empty($validation->getError('name_category'))) {
                        ?>
                <small style="color:red;"><?=$validation->getError('name_category')?></small>
                <?php }?>
            </div>
            <div class="my-4 flex space-x-2">
                    <a href="/category" class="btn btn-secondary">Cancel</a>
                    <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
        </form>
    </div>
</div>
<?= $this->endSection() ?>
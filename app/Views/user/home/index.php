<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>

<?php
if(!empty(session()->getFlashdata('msg'))){ ?>


<div class="my-4 w-full py-2 px-6 border border-green-500 bg-green-400 text-white rounded-md">
    <?php echo session()->getFlashdata('msg');?>
</div>

<?php } ?>

<!-- Jumbotron -->
<section class="my-8">
    <div class="bg-black h-80 w-full bg-gray-300 rounded overflow-hidden relative">

    </div>
</section>
<!-- Jumbotron -->

<!-- This New Products -->
<section class="my-16">
    <h3 class="text-lg font-semibold">New Products</h3>
    <div class="mt-2">

        <!-- Show New Product -->
        <div class='grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 gap-4'>
            <?php echo view('card');?>
        </div>
        <!-- End Show -->

    </div>
</section>
<!-- New Products -->

<?= $this->endSection()?>

<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>

<?php
if(!empty(session()->getFlashdata('msg'))){ ?>


<div class="my-4 w-full py-2 px-6 border border-green-500 bg-green-400 text-white rounded-md">
    <?php echo session()->getFlashdata('msg');?>
</div>

<?php } ?>

<section class="my-8">
    <h1 class="text-center text-4xl font-bold">Cetak Dokumen Tugasmu Disini</h1>
    <p class="text-center text-lg">kirimkan filemu, mari kami bantu kamu untuk print tugasmu</p>
</section>

<form action="/cetakdisini/order" method="post" class="mb-48" enctype="multipart/form-data"> 
    <section class="my-16">
        <div class="flex flex-col space-y-6">
            <div class="w-full border">
                <div class="border-b p-4">
                    <h3 class="text-lg font-semibold">Isi dulu yuks</h3>
                </div>
                <div class="p-4">
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">File</label>
                        <small class="text-gray-400 mb-4">*lampirkan file design anda disini</small>
                        <input type="file" name="file_tugas"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700">
                        <?php 
                        if(!empty($validation->getError('file_tugas'))) {
                                ?>
                            <small style="color:red;"><?=$validation->getError('file_tugas')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Berapa lembar?</label>
                        <input type="number" name="lembar_file" value="1"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700">
                        <!-- <?php 
                            if(!empty($validation->getError('lembar_file'))) {
                        ?>
                        <small style="color:red;"><?=$validation->getError('lembar_file')?></small>
                        <?php }?> -->
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Ukuran Kertas</label>
                        <select name="ukuran_kertas" id=""
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700">
                            <option value="A4">A4</option>
                            <option value="HVS">HVS</option>
                        </select>
                        <!-- <?php 
                            if(!empty($validation->getError('ukuran_kertas'))) {
                        ?>
                        <small style="color:red;"><?=$validation->getError('ukuran_kertas')?></small>
                        <?php }?> -->
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Warna Kertas</label>
                        <select name="warna_kertas" id=""
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700">
                            <option value="Hitam">Hitam</option>
                            <option value="Berwarna">Berwarna</option>
                        </select>
                        <!-- <?php 
                            if(!empty($validation->getError('warna_kertas'))) {
                        ?>
                        <small style="color:red;"><?=$validation->getError('warna_kertas')?></small>
                        <?php }?> -->
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Alamat pengiriman</label>
                        <input type="text" name="alamat_pengiriman" name="Alamat Pengantaran"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('alamat_pengiriman'))){ echo "border-red-700";}?>">
                        <?php 
                            if(!empty($validation->getError('alamat_pengiriman'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('alamat_pengiriman')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Provinsi</label>
                        <input type="text" name="provinsi" placeholder="Masukan provinsi"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                        <?php 
                            if(!empty($validation->getError('provinsi'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('provinsi')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Kabupaten</label>
                        <input type="text" name="kabupaten" placeholder="Masukan Kabupaten"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                        <?php 
                            if(!empty($validation->getError('kabupaten'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('kabupaten')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Kecamatan</label>
                        <input type="text" name="kecamatan" placeholder="Masukan kecamatan"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                        <?php 
                            if(!empty($validation->getError('kecamatan'))) {
                        ?>
                        <small style="color:red;"><?=$validation->getError('kecamatan')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Kode Pos</label>
                        <input type="text" name="kodepos" placeholder="Masukan kodepos"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('provinsi'))){ echo "border-red-700";}?>">
                        <?php 
                            if(!empty($validation->getError('kodepos'))) {
                    ?>
                        <small style="color:red;"><?=$validation->getError('kodepos')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Detail Alamat Tambahan (Optional)</label>
                        <small class="mb-2 text-gray-400">*misalnya pagarnya warna merah, samping kantor desan atau
                            lainnya</small>
                        <textarea name="detail_alamat"
                            class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?= $this->endSection()?>

    <?= $this->section('footer')?>

    <footer class="bg-white border p-6 fixed w-full bottom-0 shadow z-40">
        <div class="max-w-screen-sm md:max-w-screen-md lg:max-w-screen-lg mx-4 md:mx-12 lg:mx-auto">
            <div class="flex items-center">
                <div>
                    <!-- <table class="">
                        <tbody>
                            <tr>
                                <td class="font-bold uppercase">TOTAL BELANJA</td>
                                <td>:</td>
                                <td>Rp. -</td>
                            </tr>
                        </tbody>
                    </table> -->
                </div>
                <div class="ml-auto">
                    <button type="submit" class="py-1 px-2 bg-red-700 text-white rounded">Kirim sekarang</button>
                </div>
            </div>
        </div>
    </footer>

    <?= $this->endSection()?>
</form>
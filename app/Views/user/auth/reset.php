<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register User Forsila Creative</title>
    <!-- Tailwind CSS -->
    <link rel="stylesheet" href="<?=base_url('admin/assets/css/tailwind.css')?>">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?=base_url('admin/assets/css/main.css')?>">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
        crossorigin="anonymous" />
</head>

<body>

    <section class="flex justify-center  mt-24">
        <div class="bg-white flex items-center w-2/6 rounded-md  border border-gray-200 shadow">
            <div class="p-6 w-full">
                <h3 class="font-semibold text-lg text-center mb-8">Ganti Password</h3>
                <form action="/forgotpassword/prosesreset" method="post">
                    <?php
                    if(!empty(session()->getFlashdata('msg'))){ ?>

                    <div class="my-4 w-full py-2 px-6 border border-gray-200 bg-gray-100">
                        <?php echo session()->getFlashdata('msg');?>
                    </div>

                    <?php } ?>
                    <div class="my-6 flex flex-col">
                        <label for="" class="text-gray-400">Password Baru</label>
                        <input type="password" name="password"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded <?php if(!empty($validation->getError('password'))){ echo "invalid";}?>"
                            placeholder="Password" >
                        <?php 
                            if(!empty($validation->getError('password'))) {
                        ?>
                        <small style="color:red;"><?=$validation->getError('password')?></small>
                        <?php }?>
                        <input type="hidden" name="kw" value="<?=$_GET['kw'];?>">
                    </div>
                    <div class="my-6 flex flex-col">
                        <label for="" class="text-gray-400">Ulangi password</label>
                        <input type="password" name="repeat_password"
                            class="py-2 px-4 border border-gray-200 focus:outline-none focus:border-blue-600 rounded"
                            placeholder="Ulangi Password">
                    </div>
                    <!-- <div class="my-4 text-center">
                         <p>Kembali <a href="/login" class="font-bold">Login</a></p>
                         <p>Lanjut <a href="/" class="font-bold">Belanja</a></p>
                    </div> -->
                    <div class="my-6">
                        <button type="submit" class="w-full btn btn-primary">Reset</button>
                    </div>
                </form>
            </div>
        </div>
    </section>


    <!-- Javascript Main -->
    <script src="<?=base_url('admin/assets/js/main.js')?>"></script>

</body>

</html>
<?= $this->extend('layout/layout_user') ?>

<?= $this->section('content') ?>




<!-- This New Products -->
<section class="my-6 lg:my-16">
    <div class="flex space-x-6">
        <?php 
            echo view('user/profile/left_menu.php');
        ?>
        <div class="flex-1">
        <?php
        if(!empty(session()->getFlashdata('sukses'))){ ?>


        <div class="my-4 w-full py-2 px-6 border border-green-500 bg-green-400 text-white rounded-md">
            <?php echo session()->getFlashdata('sukses');?>
        </div>

        <?php } ?>
        <?php
        if(!empty(session()->getFlashdata('error'))){ ?>


        <div class="my-4 w-full py-2 px-6 border border-red-500 bg-red-400 text-white rounded-md">
            <?php echo session()->getFlashdata('error');?>
        </div>

        <?php } ?>
            <h3 class="font-semibold uppercase">Ganti Password</h3>
            <form action="/profile/prosesgantipassword" method="post">
                <div class="mt-4">
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Password Lama</label>
                        <input type="password" name="password_lama" placeholder="Masukan password lama" class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700">
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Password Baru</label>
                        <input type="password" name="password_baru" placeholder="Masukan password baru" class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('password_baru'))){ echo "border-red-700";}?>">
                        <?php 
                                if(!empty($validation->getError('password_baru'))) {
                        ?>
                            <small style="color:red;"><?=$validation->getError('password_baru')?></small>
                        <?php }?>
                    </div>
                    <div class="mb-6 flex flex-col">
                        <label for="" class="text-gray-400">Ulangi Password Baru</label>
                        <input type="password" name="repeat_password" placeholder="Repeat password" class="py-2 px-6 border rounded-md border-gray-300 focus:outline-none focus:border-red-700 <?php if(!empty($validation->getError('repeat_password'))){ echo "border-red-700";}?>">
                        <?php 
                                if(!empty($validation->getError('repeat_password'))) {
                        ?>
                            <small style="color:red;"><?=$validation->getError('repeat_password')?></small>
                        <?php }?>
                    </div>
                    <div class="my-4">
                         <button type="submit" class="w-full py-2 px-4 bg-red-700 rounded-md font-semibold text-white">Ganti Password</button>            
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>
<!-- New Products -->

<?= $this->endSection()?>


<?= $this->section('footer')?>

<?=view('layout/user/footer')?>

<?= $this->endSection()?>
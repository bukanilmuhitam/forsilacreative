<?php 
$uri = $_SERVER['REQUEST_URI'];
$getUri = str_replace('/' , '' , $uri);
?>
<div class="hidden md:block w-64">
    <div class="p-4 border-b border-t border-l border-r">
        <h3 class="font-bold text-gray-400 text-md">Menu :</h3>
    </div>
    <div class="p-4 border-b border-l border-r">
        <a href="/profile" class="<?=$getUri == 'profile' ?  'text-red-700': ''?>">Transaksi </a>
    </div>
    <!-- <div class="p-4 border-b border-l border-r">
        <a href="/profile/edit" class="<?=$getUri == '/profile/edit' ?  'text-red-700': ''?>">Edit Profile</a>
    </div> -->
    <div class="p-4 border-b border-l border-r">
        <a href="/profile/gantipassword" class="<?=$getUri == 'profilegantipassword' ?  'text-red-700': ''?>">Ganti Password</a>
    </div>
</div>
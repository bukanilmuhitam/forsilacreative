
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=empty($title) ? 'Forsila Creative Dashboard' : $title?></title>

    <!-- Tailwind CSS -->
    <link rel="stylesheet" href="<?=base_url('admin/assets/css/tailwind.css')?>">
    <!-- Main CSS -->
    <link rel="stylesheet" href="<?=base_url('admin/assets/css/main.css')?>">
    <!-- Fontawesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css"
        integrity="sha512-+4zCK9k+qNFUR5X+cKL9EIR+ZOhtIloNl9GIKS57V1MyNsYpYcUrUeQc9vNfzsWfV28IaLL3i96P9sdNyeRssA=="
        crossorigin="anonymous" />

</head>

<body>

    <section class="h-screen flex flex-col">
        <!-- Header -->
        <?php 
            include ('admin/nav.php');
        ?>
        <!-- End Header -->

        <div class="h-screen overflow-hidden">
            <div class="flex h-screen">
                <!-- Sidebar -->
               <?php
                include ('admin/sidebar.php');
               ?>
               <!-- End Sidebar -->
                <div class="flex-1 overflow-y-auto" id="scroll">

                    <!-- Breadchumb -->
                        <?= $this->renderSection('breadchumb') ?>
                    <!-- Endbreadchumb -->

                    <!-- Main Content -->
                    <div class="mb-40">
                        <div class="my-6 mx-6 ">
                            <?= $this->renderSection('content') ?>
                        </div>

                        <footer class="fixed bottom-0 bg-white w-full border border-gray-200 text-center lg:text-left">
                            <div class="py-6 px-6">
                                <p>Copyright &copy; <?=date('Y')?> <span class="font-semibold">Forsila Creative all reserved.</span></p>
                            </div>
                        </footer>


                    </div>
                    <!-- Main Content -->


                </div>
            </div>
        </div>

    </section>


    <!-- Javascript Main -->
    <script src="<?=base_url('admin/assets/js/main.js')?>"></script>
  

</body>

</html>
<header class="bg-white sticky top-0 shadow lg:shadow-none z-40">
    <nav class="flex flex-col">
        <div class="p-4 lg:p-8 flex items-center mx-0 md:mx-8">
            <a href="/" class="hidden md:block font-bold uppercase">forsila creative</a>
            <form action="/search/product" method="POST" class="w-full md:w-7/12  mx-0 md:mx-auto flex items-center mr-6 md:mr-0">
                <input type="text" name="textsearch"
                    class="py-1 px-4 border border-gray-200 text-sm rounded w-full focus:outline-none focus:border-red-700"
                    placeholder="Mau cetak apa hari ini?">
                    <button type="submit" class="flex items-center py-1 px-2 bg-red-700 text-white rounded-md text-xs -ml-10">Cari</button>
            </form>
            <ul class="flex space-x-2 items-center ml-auto">
                <li class="mr-6">
                    <a href="/order" class="relative">
                        <i class="fas fa-shopping-cart fa-lg"></i>
                        <?php 
                            if($countcart > 0){
                        ?>
                        <span
                            class="absolute h-6 w-6 bg-red-700 text-white text-center my-auto rounded-full top-0 -mt-4 right-0 -mr-2"><?=$countcart?></span>
                        <?php }?>
                    </a>
                </li>
                <li class="block lg:hidden">
                    <button type="button" class="focus:outline-none open-side">
                        <i class="fa fa-bars fa-lg"></i>
                    </button>
                </li>
                <?php if(!empty(session()->get('login_user'))){?>
                    <li class="hidden lg:block">
                        <a href="/profile" class="text-semibold capitalize"><i class="fa fa-user"></i>&nbsp;profile</a>
                    </li>
                    <li class="hidden lg:block">
                        <a href="/logout" class="py-1 px-4 text-sm bg-red-700 rounded-md font-semibold text-white">Logout</a>
                    </li>
                <?php }else{?>
                <li class="hidden lg:block">
                    <a href="/register" class="py-1 px-4 text-sm border border-red-700 rounded-md font-semibold">Daftar</a>
                </li>
                <li class="hidden lg:block">
                    <a href="/login" class="py-1 px-4 text-sm bg-red-700 rounded-md font-semibold text-white">Login</a>
                </li>
                <?php }?>
            </ul>
        </div>
        <div class="hidden lg:block p-4 bg-gray-100">
            <div class="mx-0 md:mx-12">
                <ul class="hidden lg:flex space-x-6 mx-auto capitalize font-semibold text-sm items-center">
                    <li>
                        <a href="/">home</a>
                    </li>
                    <?php 
                        foreach($menus as $menu){
                    ?>
                    <li class="relative dropmenu">
                        <span><?=$menu['nama_category'];?></span>
                        <ul class="p-4 w-48 absolute bg-white shadow flex flex-col space-y-2 font-normal dropcontent">
                            <?php 
                                foreach($submenus as $submenu){
                                    if($menu['id'] == $submenu['id_category']){
                            ?>
                                <li>
                                    <a href="/subcategory/product/<?=$submenu['permalink'];?>"><?=$submenu['nama_subcategory'];?></a>
                                </li>
                            <?php }}?>
                        </ul>
                    </li>
                    <?php }?>
                    <li>
                        <a href="/cetakdisini">#cetakdisini</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</header>
<!-- End Navbar -->
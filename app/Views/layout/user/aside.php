<aside class="lg:hidden h-screen bg-red-700 w-full md:w-1/2 absolute z-40 text-white overflow-hidden" id="sidebar">
    <div class="p-4 flex items-center mx-6">
        <a href="/" class="uppercase font-bold">forsila creative</a>
        <button type="button" class="ml-auto font-sm focus:outline-none close-side"><i
                class="fa fa-times fa-lg"></i></button>
    </div>
    <!-- menu -->
    <div class="p-4 mx-6 h-screen overflow-y-auto">
        <ul class="flex flex-col space-y-4">
            <li class="menu-content">
                <a href="/" class="flex spcae-x-3">
                    <span class="font-semibold">Home</span>
                </a>
            </li>
            <?php 
                        foreach($menus as $menu){
            ?>
            <li class="menu-content ">
                <button type="button" class="flex justify-between w-full focus:outline-none menubtn-lv1">
                    <div class="flex items-center space-x-2">
                        <span class="font-semibold capitalize"><?=$menu['nama_category'];?></span>
                    </div>
                    <div class="flex items-center">
                        <i class="fa fa-angle-down fa-sm ic-toogle"></i>
                    </div>
                </button>
                <ul class="mt-3 flex flex-col space-y-3 treemenu">
                    <?php 
                        foreach($submenus as $submenu){
                            if($menu['id'] == $submenu['id_category']){
                    ?>
                    <li class="ml-1">
                        <a href="/subcategory/product/<?=$submenu['permalink'];?>" class="flex items-center space-x-2">
                            <span class="font-normal text-sm"><?=$submenu['nama_subcategory'];?></span>
                        </a>
                    </li>
                    <?php }}?>
                </ul>
            </li>
            <?php }?>
            <li class="menu-content">
                <div class="flex justify-center space-x-4 mt-6">
                    <?php if(!empty(session()->get('login_user'))){?>
                        <a href="/logout" class="py-1 px-4 text-sm border border-white rounded-md  ont-semibold">Logout</a>
                    <?php }else{?>
                        <a href="/login" class="py-1 px-4 text-sm border border-white rounded-md  ont-semibold">Login</a>
                        <a href="/register" class="py-1 px-4 text-sm bg-white rounded-md  font-semibold text-black">Daftar</a>
                    <?php }?>
                </div>
            </li>
        </ul>
    </div>
    <!-- end menu -->
</aside>
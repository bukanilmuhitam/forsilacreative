<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=empty($title) ? 'Percetakan Forsila Creative' : $title?></title>
    <link rel="stylesheet" href="<?=base_url('user/assets/css/output.css')?>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link rel="stylesheet" href="<?=base_url('user/assets/css/main.css')?>">
</head>

<body>

    <section class="relative" id="app">
        <!-- Aside -->
        <?php 
    include "user/aside.php";
?>
        <!-- Aside -->
        <!-- Navbar -->
        <?php 
    include "user/navbar.php";
?>
        <!-- Navbar -->

        <main>

            <section class="max-w-screen-sm md:max-w-screen-md lg:max-w-screen-lg mx-4 md:mx-12 lg:mx-auto">
                <?= $this->renderSection('content') ?>
            </section>

        </main>

        <?= $this->renderSection('footer') ?>

    </section>
    <!-- JS MAIN -->
    <script src="<?=base_url('user/assets/js/main.js')?>"></script>

    <!-- VUE JS -->
    <script src="https://unpkg.com/vue@next"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>

    <script type="text/javascript">
    window.general = <?php
          echo json_encode([
              'base_url' => base_url(),
              'uri' => $_SERVER["REQUEST_URI"],
          ]);
    ?>
    </script>
    <script src="<?=base_url('vuejsfile/userfile.js')?>"></script>


</body>

</html>
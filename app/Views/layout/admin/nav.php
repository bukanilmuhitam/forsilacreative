<header class="flex bg-white items-center">
    <a href="./index.html" class="flex w-0 p-0  lg:w-72 lg:p-4 bg-blue-600 text-white">
        <span class="font-semibold uppercase">Admin Forsila Creative</span>

    </a>
    <nav class="flex flex-1 mx-0 lg:mx-2 p-4">
        <button type="button" onclick="hideside()" class="text-right hidden lg:block focus:outline-none">
            <i class="fas fa-bars"></i>
        </button>
        <button type="button" onclick="tampilsidemenu()" class="text-right block lg:hidden focus:outline-none">
            <i class="fas fa-bars icon-bar"></i>
        </button>
        <ul class="flex space-x-6 text-md font-light items-center ml-auto">
            <!-- <li class="relative dropmenu">
                <i class="fa fa-bell"></i>
                <span class="bg-red-600 py-1 px-1 rounded text-white absolute top-0 -ml-2 -mt-1 bubble-notif">5</span>
                <div class="dropcontent bg-white border border-gray-200  w-52 lg:w-60 -ml-48 lg:-ml-56 h-auto rounded ">
                    <div class="my-4 mx-4 ">
                        <a href="" class="flex space-x-2">
                            <div class="flex items-center">
                                <i class="fa fa-envelope fa-md"></i>
                            </div>
                            <div class="flex items-center">
                                <p>Message (15)</p>
                            </div>
                        </a>
                    </div>
                    <div class="py-2 px-6 border-t-2 border-gray-100 text-center text-sm">
                        <a href="">See all notification</a>
                    </div>
                </div>
            </li> -->
            <!-- <li class="relative dropmenu">
                <i class="fas fa-cogs"></i>
                <div class="dropcontent bg-white border border-gray-200  w-52 lg:w-60 -ml-48 lg:-ml-56 h-auto rounded ">
                    <div class="my-4 mx-4">
                        <a href="" class="">
                            Profile Setting
                        </a>
                    </div>
                    <div class="my-4 mx-4">
                        <a href="" class="">
                            Password Setting
                        </a>
                    </div>
                </div>
            </li> -->
            <li>
                <a href="/auth/logout"><i class="fas fa-power-off"></i></a>
            </li>
        </ul>
    </nav>
</header>
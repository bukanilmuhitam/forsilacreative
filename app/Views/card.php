<?php 
                        foreach($products as $product){
?>
                    <div class="w-full border border-gray-200 shadow">
                        <a href="/produk/detail/<?=$product['permalink_product']?>" class="overflow-hidden">
                            <img src="<?=base_url()?>/upload/product/<?=$product['gambar_produk'];?>"
                                alt="<?=$product['nama_produk'];?>">
                        </a>
                        <div class="p-4">
                            <a href="/produk/detail/<?=$product['permalink_product']?>" class="overflow-hidden">
                                <h3
                                    class="capitalize text-gray-700 text-md overflow-hidden overflow-ellipsis whitespace-nowrap w-full inline-block mb-0">
                                    <?=$product['nama_produk'];?></h3>
                            </a>
                            <p class="text-sm font-semibold mb-4">Rp. <?=number_format($product['harga_default_pcs'])?>
                            </p>

                            <!-- <?php 
                            $subcategory = $product['sub_category'];
                            foreach($submenus as $submenu){
                                if($submenu['id'] == $subcategory){
                                    $idcategory = $submenu['id_category'];
                                    foreach($menus as $menu){
                                        if($idcategory == $menu['id']){
                        ?>
                            <a href="/category/<?=$menu['permalink'];?>"
                                class="bg-red-700 text-white py-1 px-4 rounded-full text-sm capitalize"><?=$menu['nama_category'];?></a>
                        <?php
                                    }
                                }
                            }
                        }
                         ?> -->
                        </div>

                    </div>
                    <?php }?>
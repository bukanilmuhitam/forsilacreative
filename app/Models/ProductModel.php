<?php namespace App\Models;

use CodeIgniter\Model;

class ProductModel extends Model
{

    protected $table    = 'list_produk';
    protected $primaryKey = 'id';

    protected $allowedFields = ['sub_category' , 'nama_produk' , 'materials' , 'kuantitas' , 'estimasi_selesai_produksi' ,'gambar_produk' , 'deskripsi_produk' , 'type_ukuran' , 'ukuran_produk' , 'harga_default_pcs' , 'permalink_product' , 'berat_product'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';


    function getData($id= '' , $page = ''){
        $pager = \Config\Services::pager('');
        if($page == ''){
            if($id == ''){
                return $this->orderBy('id' , 'DESC')->findAll();
            }else{
                return $this->where('id' , $id)->first();
            }
        }else{
            $data = [
                'items' => $this->orderBy('id' , 'DESC')->paginate($page , 'bootstrap'),
                'pager' => $this->pager,
            ];

            return $data;
        }
    }
    
    function newProduct(){
        return $this->orderBy('id' , 'DESC')->limit(4)->findAll();
    }

    function getProductBySubcategory($id){
        return $this->orderBy('id' , 'DESC')->where('sub_category' , $id)->findAll();
    }

    function gerProductByPermalink($id){
        return $this->where('permalink_product' , $id)->first();
    }
    
    public function searchdata($value){
        return $this->like('nama_produk' , $value)->findAll();
    }

    function joinOrder(){

        $db      = \Config\Database::connect();
        $builder = $db->table('projects')
        ->select('*')
        ->join('orders' , 'orders.id = projects.id_order') 
        ->get();
        return $builder;


    }

    function saveData($data){
        $query = $this->insert($data);
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

    function updatedata($id , $data){

       $query = $this->update($id , $data);
       if($query){
           return 'ok';
       }else{
           return 'error';
       }

    }

    function deletedata($id){
        $query = $this->where('id', $id)->delete();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

}
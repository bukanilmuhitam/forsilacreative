<?php namespace App\Models;

use CodeIgniter\Model;

class OrderModel extends Model
{

    protected $table    = 'product_order';
    protected $primaryKey = 'id';

    protected $allowedFields = ['number_invoice' , 'product_id' , 'username' , 'alamat_pengiriman' , 'provinsi' , 'kabupaten' , 'kecamatan' , 'kode_pos' , 'keterangan_tambahan_pengiriman' , 'tshipping' , 'tpayment' , 'catatan_tambahan' , 'status_order' , 'date_order' , 'qty_perproduct' , 'lampiran_file'];

    protected $useTimestamps = false;
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';


    function getData($id= '' , $page = ''){
        $pager = \Config\Services::pager('');
        if($page == ''){
            if($id == ''){
                return $this->orderBy('id' , 'DESC')->findAll();
            }else{
                return $this->where('id' , $id)->first();
            }
        }else{
            $data = [
                'items' => $this->orderBy('id' , 'DESC')->paginate($page , 'bootstrap'),
                'pager' => $this->pager,
            ];

            return $data;
        }
    }
    
    function getDataByUsername($username){
        return $this->where('username' , $username)->first();
    }

    function getDataByInvoiceNumber($number_invoice){
        return $this->where('number_invoice' , $number_invoice)->first();
    }

    function check_cart($username = ''){
        return $this->where('username' , $username)->findAll();
    }

    function updateStatusOrder($id , $status){
        $query = $this->update($id , [
            'status_order' => $status
        ]);
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

    function deleteByIdproduct($id){
        $query = $this->where('id_product', $id)->delete();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }
    
    function joinOrder(){

        $db      = \Config\Database::connect();
        $builder = $db->table('projects')
        ->select('*')
        ->join('orders' , 'orders.id = projects.id_order') 
        ->get();
        return $builder;


    }

    function saveData($data){
        $query = $this->insert($data);
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

    function updatedata($id , $data){

       $query = $this->update($id , $data);
       if($query){
           return 'ok';
       }else{
           return 'error';
       }

    }

    function updateByIdproduct($id , $data){
        $query = $this->where('id_product' , $id)->set($data)->update();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

    function deletedata($id){
        $query = $this->where('id', $id)->delete();
        if($query){
            return 'ok';
        }else{
            return 'error';
        }
    }

    function getOrderBelumProses($username){
        return $this->where('username' , $username)->where('status_order' , '0')->orderBy('id' , 'DESC')->findAll();
    }

    function getOrderProses($username){
        return $this->where('username' , $username)->where('status_order' , '1')->orderBy('id' , 'DESC')->findAll();
    }
    
    function getOrderShipping($username){
        return $this->where('username' , $username)->where('status_order' , '2')->orderBy('id' , 'DESC')->findAll();
    }

    function getOrderComplete($username){
        return $this->where('username' , $username)->where('status_order' , '3')->orderBy('id' , 'DESC')->findAll();
    }

    function getOrderBatal($username){
        return $this->where('username' , $username)->where('status_order' , '4')->orderBy('id' , 'DESC')->findAll();
    }

    function report($start , $end){
        return $this->where('date_order >=' , $start)->where('date_order <=' , $end)->orderBy('id' , 'DESC')->findAll();
    }

}